require 'swagger_helper'

RSpec.describe 'api/v1/unit_offices', type: :request do
  path '/api/v1/unit_offices/autocomplete' do
    get('autocomplete Unit Office') do
      tags 'Unit Offices'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/unit_offices' do
    get('List Unit Offices') do
      tags 'Unit Offices'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Unit Office') do
      tags 'Unit Offices'
      security [Bearer: {}]

      parameter name: :unit_office, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Unit Batuindah',
          "code": 'BE-001',
          "city_id": '5ec209f8e64d1e4015fb02a1',
          "branch_office_id": '5f27e399d77fab5a42fc2d88',
          "address": 'Cinunuk',
          "longitude": 24.555,
          "latitude": 29.555,
          "description": 'Unit Cabanf Cileunyi'
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/unit_offices/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Unit Office') do
      tags 'Unit Offices'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Unit Office') do
      tags 'Unit Offices'
      security [Bearer: {}]

      parameter name: :unit_office, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Unit Batuindah',
          "code": 'BE-001',
          "city_id": '5ec209f8e64d1e4015fb02a1',
          "branch_office_id": '5f27e399d77fab5a42fc2d88',
          "address": 'Cinunuk',
          "longitude": 24.555,
          "latitude": 29.555,
          "description": 'Unit Cabanf Cileunyi'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Unit Office') do
      tags 'Unit Offices'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
