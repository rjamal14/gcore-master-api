require 'swagger_helper'

RSpec.describe 'api/v1/customer_jobs', type: :request do
  path '/api/v1/customer_jobs/autocomplete' do
    get('Autocomplete Customer Job') do
      tags 'Customer Jobs'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer_jobs' do
    get('List Customer Jobs') do
      tags 'Customer Jobs'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Customer Job') do
      tags 'Customer Jobs'
      security [Bearer: {}]

      parameter name: :customer_jobs, in: :body, schema: {
        type: :object,
        example: {
          "customer_jobs": {
            "name": 'Petani',
            "description": 'Petani Di sawah'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer_jobs/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Customer Job') do
      tags 'Customer Jobs'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Customer Job') do
      tags 'Customer Jobs'
      security [Bearer: {}]

      parameter name: :customer_jobs, in: :body, schema: {
        type: :object,
        example: {
          "customer_jobs": {
            "name": 'Petani',
            "description": 'Petani Di sawah'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Customer Job') do
      tags 'Customer Jobs'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
