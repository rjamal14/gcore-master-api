require 'swagger_helper'

RSpec.describe 'api/v1/countries', type: :request do
  path '/api/v1/countries/autocomplete' do
    get('Autocomplete Country') do
      tags 'Countries'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/countries' do
    get('Get List Countries') do
      tags 'Countries'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Country') do
      tags 'Countries'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "country": {
            "name": 'India',
            "nationality": 'Indian'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/countries/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Country') do
      tags 'Countries'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Country') do
      tags 'Countries'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "country": {
            "name": 'Indonesia',
            "nationality": 'NKRI'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Country') do
      tags 'Countries'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
