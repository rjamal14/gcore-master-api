require 'swagger_helper'

RSpec.describe 'api/v1/product_period', type: :request do
  path '/api/v1/product/{product_id}/product_period' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'Product ID'

    get('List Product Period') do
      tags 'Product Period'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Period') do
      tags 'Product Period'
      security [Bearer: {}]
      parameter name: :area, in: :body, schema: {
        type: :object,
        example: {
          "product_period": {
            "province": '',
            "period_of_time": '2',
            "due_date": '1',
            "grace_period": '1',
            "fine_amount": 10.98,
            "fine_percentage": 0.18
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_period/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Period') do
      tags 'Product Period'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Period') do
      tags 'Product Period'
      security [Bearer: {}]
      parameter name: :area, in: :body, schema: {
        type: :object,
        example: {
          "product_period": {
            "province": '',
            "period_of_time": '2',
            "due_date": '1',
            "grace_period": '1',
            "fine_amount": 10.98,
            "fine_percentage": 0.18
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Period') do
      tags 'Product Period'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
