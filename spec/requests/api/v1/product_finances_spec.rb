require 'swagger_helper'

RSpec.describe 'api/v1/product_finances', type: :request do
  path '/api/v1/product_finances' do
    get('Lilst Product Insurance') do
      tags 'Product Finances'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Finances') do
      tags 'Product Finances'
      security [Bearer: {}]
      parameter name: :product_finance, in: :body, schema: {
        type: :object,
        example: {
          "product_finance": {
            "product_id": '5efc44ad7586f21bcde7d1b4',
            "min_weight": 1.5,
            "min_karatase": 18,
            "min_loan": 10_000,
            "max_loan": 20_000,
            "insurance_item_id": '5ef300d77586f25e4965ce20',
            "product_finance_items_attributes": [
              {
                "regional_id": '5f69f0efd77fab146aaca56d',
                "stle_value": 650_000,
                "ltv_value": 80
              },
              {
                "insurance_item_id": '5ef300e17586f25e4965ce21',
                "stle_value": 800_000,
                "ltv_value": 90,
                "min_karatase": 18,
                "min_weight": 1.8,
                "min_loan": 100_000,
                "max_loan": 200_000
              }
            ]
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product_finances/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Finances') do
      tags 'Product Finances'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Finances') do
      tags 'Product Finances'
      security [Bearer: {}]
      parameter name: :product_finance, in: :body, schema: {
        type: :object,
        example: {
          "product_finance": {
            "product_id": '5efc44ad7586f21bcde7d1b4',
            "min_weight": 1.5,
            "min_karatase": 18,
            "min_loan": 10_000,
            "max_loan": 20_000,
            "insurance_item_id": '5ef300d77586f25e4965ce20',
            "product_finance_items_attributes": [
              {
                "regional_id": '5f69f0efd77fab146aaca56d',
                "stle_value": 650_000,
                "ltv_value": 80
              },
              {
                "insurance_item_id": '5ef300e17586f25e4965ce21',
                "stle_value": 800_000,
                "ltv_value": 90,
                "min_karatase": 18,
                "min_weight": 1.8,
                "min_loan": 100_000,
                "max_loan": 200_000
              }
            ]
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Finances') do
      tags 'Product Finances'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
