require 'swagger_helper'

RSpec.describe 'api/v1/product_admin_cost_discount', type: :request do
  path '/api/v1/product/{product_id}/product_admin_cost_discount' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'

    get('List Product Admin Cost Discounts') do
      tags 'Product Admin Cost Discounts'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Admin Cost Discounts') do
      tags 'Product Admin Cost Discounts'
      security [Bearer: {}]
      parameter name: :product_admin_cost_discount, in: :body, schema: {
        type: :object,
        example: {
          "product_admin_cost_discount": {
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "repawn_1": 20,
            "repawn_2": 20,
            "repawn_3": 20,
            "repawn_4": 20,
            "repawn_5": 20,
            "repawn_6": 20,
            "repawn_7": 20,
            "repawn_8": 20,
            "repawn_9": 20,
            "repawn_10": 20,
            "repawn_11": 20,
            "repawn_12": 20
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_admin_cost_discount/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Admin Cost Discounts') do
      tags 'Product Admin Cost Discounts'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Admin Cost Discounts') do
      tags 'Product Admin Cost Discounts'
      security [Bearer: {}]
      parameter name: :product_admin_cost_discount, in: :body, schema: {
        type: :object,
        example: {
          "product_admin_cost_discount": {
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "repawn_1": 20,
            "repawn_2": 20,
            "repawn_3": 20,
            "repawn_4": 20,
            "repawn_5": 20,
            "repawn_6": 20,
            "repawn_7": 20,
            "repawn_8": 20,
            "repawn_9": 20,
            "repawn_10": 20,
            "repawn_11": 20,
            "repawn_12": 20
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Admin Cost Discounts') do
      tags 'Product Admin Cost Discounts'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
