require 'swagger_helper'

RSpec.describe 'api/v1/product_rental_cost', type: :request do
  path '/api/v1/product/{product_id}/product_rental_cost' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'

    get('List Product Rental Costs') do
      tags 'Product Rental Costs'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Rental Costs') do
      tags 'Product Rental Costs'
      security [Bearer: {}]
      parameter name: :product_rental_cost, in: :body, schema: {
        type: :object,
        example: {
          "product_rental_cost": {
            "rental_cost_percentage": 2,
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "day_period": 14
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_rental_cost/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Rental Costs') do
      tags 'Product Rental Costs'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Rental Costs') do
      tags 'Product Rental Costs'
      security [Bearer: {}]
      parameter name: :product_rental_cost, in: :body, schema: {
        type: :object,
        example: {
          "product_rental_cost": {
            "rental_cost_percentage": 2,
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "day_period": 14
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Rental Costs') do
      tags 'Product Rental Costs'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
