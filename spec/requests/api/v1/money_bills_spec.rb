require 'swagger_helper'

RSpec.describe 'api/v1/money_bills', type: :request do
  path '/api/v1/money_bills/autocomplete' do
    get('Autocomplete Money Bills') do
      tags 'Money Bills'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/money_bills/list' do
    get('List All Money Bills') do
      tags 'Money Bills'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/money_bills' do
    get('List Money Bills') do
      tags 'Money Bills'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Money Bills') do
      tags 'Money Bills'
      security [Bearer: {}]
      parameter name: :money_bill, in: :body, schema: {
        type: :object,
        example: {
          "money_bill": {
            "nominal": 1000,
            "description": 'Uang recehan 1000',
            "money_type": 'coin'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/money_bills/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Money Bills') do
      tags 'Money Bills'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Money Bills') do
      tags 'Money Bills'
      security [Bearer: {}]
      parameter name: :money_bill, in: :body, schema: {
        type: :object,
        example: {
          "money_bill": {
            "nominal": 1000,
            "description": 'Uang recehan 1000',
            "money_type": 'coin'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Money Bills') do
      tags 'Money Bills'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
