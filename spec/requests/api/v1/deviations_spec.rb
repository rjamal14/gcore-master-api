require 'swagger_helper'

RSpec.describe 'api/v1/deviations', type: :request do
  path '/api/v1/product/{product_id}/deviation/{insurance_item_id}/{regional_id}/{loan_amount}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'insurance_item_id', in: :path, type: :string, description: 'insurance_item_id'
    parameter name: 'regional_id', in: :path, type: :string, description: 'regional_id'
    parameter name: 'loan_amount', in: :path, type: :string, description: 'loan_amount'

    get('Check Deviation Data') do
      tags 'Deviations'
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:insurance_item_id) { '123' }
        let(:regional_id) { '123' }
        let(:loan_amount) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
