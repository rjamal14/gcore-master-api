require 'swagger_helper'

describe 'Cities' do
  path '/api/v1/cities/index_by_provinces' do
    get('index_by_province city') do
      tags 'Cities'
      parameter name: :province_id, in: :query, type: :string, description: 'province id'
      security [Bearer: {}]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/cities/autocomplete' do
    get('autocomplete city') do
      tags 'Cities'
      parameter name: :province_id, in: :query, type: :string, description: 'filter by province code'
      parameter name: :query, in: :query, type: :string, description: 'keyword you want to search'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/cities' do
    get('list cities') do
      tags 'Cities'
      security [Bearer: {}]
      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate'
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by', enum: [:name, :created_at, :province_id]
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('create city') do
      tags 'Cities'
      security [Bearer: {}]
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          province_id: { type: :string }
        },
        required: %w[name province_id]
      }
      # parameter name: :name, in: :form, type: :string, description: 'City name you want'
      # parameter name: :province_id, in: :form, type: :string, description: 'Type of sorting'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/cities/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show city') do
      tags 'Cities'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update city') do
      tags 'Cities'
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          province_id: { type: :string }
        }
      }
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('update city') do
      tags 'Cities'
      security [Bearer: {}]
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          province_id: { type: :string }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete city') do
      tags 'Cities'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
