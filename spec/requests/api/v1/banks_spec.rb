require 'swagger_helper'

RSpec.describe 'api/v1/banks', type: :request do
  path '/api/v1/banks/autocomplete' do
    get('Autocomplete Bank') do
      tags 'Banks'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/banks' do
    get('List Banks') do
      tags 'Banks'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Bank') do
      tags 'Banks'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Bank Cinta Damai',
          "description": '117',
          "bank_code": '117'
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/banks/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Bank') do
      tags 'Banks'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Bank') do
      tags 'Banks'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Bank Cinta Damai',
          "description": '117',
          "bank_code": '117'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Bank') do
      tags 'Banks'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
