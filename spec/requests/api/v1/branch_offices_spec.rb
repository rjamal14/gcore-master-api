require 'swagger_helper'

RSpec.describe 'api/v1/branch_offices', type: :request do
  path '/api/v1/branch_offices/autocomplete' do
    get('Autocomplete Branch Office') do
      tags 'Branch Office'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/branch_offices' do
    get('List Branch Offices') do
      tags 'Branch Office'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Branch Office') do
      tags 'Branch Office'
      security [Bearer: {}]

      parameter name: :branch_office, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Cabang Medan',
          "city_id": '5eda0410e64d1e58cfd9903c',
          "code": 'MDN',
          "address": 'Jalan Belok',
          "longitude": 24.555,
          "latitude": 29.555,
          "area_id": '60192606e64d1e245ac70f70'
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/branch_offices/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Branch Office') do
      tags 'Branch Office'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Branch Office') do
      tags 'Branch Office'
      security [Bearer: {}]

      parameter name: :branch_office, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Cabang Medan',
          "city_id": '5eda0410e64d1e58cfd9903c',
          "code": 'MDN',
          "address": 'Jalan Belok',
          "longitude": 24.555,
          "latitude": 29.555,
          "area_id": '60192606e64d1e245ac70f70'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Branch Office') do
      tags 'Branch Office'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
