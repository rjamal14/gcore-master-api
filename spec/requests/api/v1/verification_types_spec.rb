require 'swagger_helper'

RSpec.describe 'api/v1/verification_types', type: :request do
  path '/api/v1/verification_types/autocomplete' do
    get('Autocomplete Verification Type') do
      tags 'Verification Types'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/verification_types' do
    get('List Verification Types') do
      tags 'Verification Types'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Verification Type') do
      tags 'Verification Types'
      security [Bearer: {}]

      parameter name: :area, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Komplits',
          "status": true,
          "verification_item_ids": [
            {
              "verification_item_id": '5ec2271be64d1e0e57a0ec0d'
            }
          ]
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/verification_types/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Verification Type') do
      tags 'Verification Types'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Verification Type') do
      tags 'Verification Types'
      security [Bearer: {}]

      parameter name: :area, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Komplits',
          "status": true,
          "verification_item_ids": [
            {
              "verification_item_id": '5ec2271be64d1e0e57a0ec0d'
            }
          ]
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Verification Type') do
      tags 'Verification Types'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
