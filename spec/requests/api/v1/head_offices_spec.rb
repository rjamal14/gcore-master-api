require 'swagger_helper'

RSpec.describe 'api/v1/head_offices', type: :request do
  path '/api/v1/head_offices' do
    get('List Head Offices') do
      tags 'Head Offices'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Head Offices') do
      tags 'Head Offices'
      security [Bearer: {}]
      parameter name: :head_office, in: :body, schema: {
        type: :object,
        example: {
          "head_office": {
            "name": 'Kantor Pusat GCDA Aceh',
            "code": 'GCDA-ACH',
            "city_id": '5f6a04d523bce5e6898465e7',
            "address": 'Jalan Batuindah XII',
            "latitude": '121.000',
            "longitude": '122.900',
            "description": 'Kantor Pusat Jawa Barat'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/head_offices/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show head_office') do
      tags 'Head Offices'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Head Offices') do
      tags 'Head Offices'
      security [Bearer: {}]
      parameter name: :head_office, in: :body, schema: {
        type: :object,
        example: {
          "head_office": {
            "name": 'Kantor Pusat GCDA Aceh',
            "code": 'GCDA-ACH',
            "city_id": '5f6a04d523bce5e6898465e7',
            "address": 'Jalan Batuindah XII',
            "latitude": '121.000',
            "longitude": '122.900',
            "description": 'Kantor Pusat Jawa Barat'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Head Offices') do
      tags 'Head Offices'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
