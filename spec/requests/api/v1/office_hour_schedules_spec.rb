require 'swagger_helper'

RSpec.describe 'api/v1/office_hour_schedules', type: :request do
  path '/api/v1/office_hour_schedules' do
    get('List Office Hour Schedules') do
      tags 'Office Hour Schedules'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Office Hour Schedules') do
      tags 'Office Hour Schedules'
      security [Bearer: {}]
      parameter name: :office_hour_schedules, in: :body, schema: {
        type: :object,
        example: {
          "office_hour_schedules": {
            "area_id": '5eafc5af3dc9fc241f7ffd170',
            "schedule_date": '2020-05-04',
            "started_at": '2020-05-04 14:12:30',
            "closed_at": '2020-05-04 14:12:30'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/office_hour_schedules/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Office Hour Schedules') do
      tags 'Office Hour Schedules'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Office Hour Schedules') do
      tags 'Office Hour Schedules'
      security [Bearer: {}]
      parameter name: :office_hour_schedules, in: :body, schema: {
        type: :object,
        example: {
          "office_hour_schedules": {
            "area_id": '5eafc5af3dc9fc241f7ffd170',
            "schedule_date": '2020-05-04',
            "started_at": '2020-05-04 14:12:30',
            "closed_at": '2020-05-04 14:12:30'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Office Hour Schedules') do
      tags 'Office Hour Schedules'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
