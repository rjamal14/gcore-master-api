require 'swagger_helper'

RSpec.describe 'api/v1/verification_items', type: :request do
  path '/api/v1/verification_items/autocomplete' do
    get('Autocomplete Verification Item') do
      tags 'Verification Items'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/verification_items' do
    get('List Verification Items') do
      tags 'Verification Items'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Verification Item') do
      response(200, 'successful') do
        tags 'Verification Items'
        security [Bearer: {}]

        parameter name: :verification_item, in: :body, schema: {
          type: :object,
          example: {
            "name": 'Kopsa',
            "status": true,
            "document_needed": 'mandatory',
            "verification_type_ids": [
              {
                "verification_type_id": '5ec1fa28e64d1e2a96ac0fa2'
              }
            ]
          }
        }
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/verification_items/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Verification Item') do
      tags 'Verification Items'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Verification Item') do
      tags 'Verification Items'
      security [Bearer: {}]

      parameter name: :verification_item, in: :body, schema: {
        type: :object,
        example: {
          "name": 'Kopsa',
          "status": true,
          "document_needed": 'mandatory',
          "verification_type_ids": [
            {
              "verification_type_id": '5ec1fa28e64d1e2a96ac0fa2'
            }
          ]
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Verification Item') do
      tags 'Verification Items'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
