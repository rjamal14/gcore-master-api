require 'swagger_helper'

RSpec.describe 'api/v1/product_insurance_item', type: :request do
  path '/api/v1/product_insurance_item/autocomplete' do
    get('Autocomplete Product Insurance Item') do
      tags 'Product Insurance Item'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_insurance_item' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'

    get('List Product Insurance Item') do
      tags 'Product Insurance Item'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Insurance Item') do
      tags 'Product Insurance Item'
      security [Bearer: {}]
      parameter name: :product_insurance_item, in: :body, schema: {
        type: :object,
        example: {
          "insurance_item_id": '5ef300e17586f25e4965ce21',
          "name": 'Asal',
          "minimum_karatase": 15,
          "certificate": 'Antam'
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_insurance_item/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Insurance Item') do
      tags 'Product Insurance Item'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Insurance Item') do
      tags 'Product Insurance Item'
      security [Bearer: {}]
      parameter name: :product_insurance_item, in: :body, schema: {
        type: :object,
        example: {
          "insurance_item_id": '5ef300e17586f25e4965ce21',
          "name": 'Asal',
          "minimum_karatase": 15,
          "certificate": 'Antam'
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Insurance Item') do
      tags 'Product Insurance Item'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
