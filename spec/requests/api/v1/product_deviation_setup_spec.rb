require 'swagger_helper'

RSpec.describe 'api/v1/product_deviation_setup', type: :request do
  path '/api/v1/product/{product_id}/product_deviation_setup' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'

    get('List Product Deviation Setup') do
      tags 'Product Deviation Setup'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Product Deviation Setup') do
      tags 'Product Deviation Setup'
      security [Bearer: {}]
      parameter name: :product_deviation_setup, in: :body, schema: {
        type: :object,
        example: {
          "product_deviation_setup": {
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "name": 'ltv',
            "branch": 17,
            "area": 18,
            "region": 30,
            "headquarters": 21
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/product/{product_id}/product_deviation_setup/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'product_id', in: :path, type: :string, description: 'product_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Product Deviation Setup') do
      tags 'Product Deviation Setup'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Product Deviation Setup') do
      tags 'Product Deviation Setup'
      security [Bearer: {}]
      parameter name: :product_deviation_setup, in: :body, schema: {
        type: :object,
        example: {
          "product_deviation_setup": {
            "insurance_item_id": '5f2b8afed77fab04426c9008',
            "name": 'ltv',
            "branch": 17,
            "area": 18,
            "region": 30,
            "headquarters": 21
          }
        }
      }
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Product Deviation Setup') do
      tags 'Product Deviation Setup'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:product_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
