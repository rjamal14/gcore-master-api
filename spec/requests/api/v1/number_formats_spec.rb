require 'swagger_helper'

RSpec.describe 'api/v1/number_formats', type: :request do
  path '/api/v1/number_formats/autocomplete' do
    get('Autocomplete Number Format') do
      tags 'Number Format'
      parameter name: :query, in: :query, type: :string, description: 'Query Search'

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/number_formats/generate' do
    get('Generate Number') do
      tags 'Number Format'
      security [Bearer: {}]

      parameter name: :type, in: :query, type: :string
      parameter name: :office_id, in: :query, type: :string
      parameter name: :office_type, in: :query, type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/number_formats' do
    get('List Number Formats') do
      tags 'Number Format'
      security [Bearer: {}]

      parameter name: :search, in: :query, type: :string, description: 'Search keyword'
      parameter name: :page, in: :query, type: :string, description: 'Page you want to navigate', default: 1
      parameter name: :per_page, in: :query, type: :string, description: 'The amount of data to be displayed', default: 10
      parameter name: :order_by, in: :query, type: :string, description: 'Attribute you want sort by'
      parameter name: :order, in: :query, type: :string, description: 'Type of sorting', enum: [:asc, :desc]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Number Format') do
      tags 'Number Format'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "name": 'SGE Number',
          "code": 'SGE',
          "num_format": 'GC-#kode_wilayah-#kode_unit-#dd#mm#yy-#increment',
          "year": '2020',
          "number_increment": '1',
          "last_number": '0',
          "leading_zero": '5',
          "description": 'Nomor SGE untuk Nasabah'
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/number_formats/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Number Format') do
      tags 'Number Format'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Number Format') do
      tags 'Number Format'
      security [Bearer: {}]

      parameter name: :country, in: :body, schema: {
        type: :object,
        example: {
          "name": 'SGE Number',
          "code": 'SGE',
          "num_format": 'GC-#kode_wilayah-#kode_unit-#dd#mm#yy-#increment',
          "year": '2020',
          "number_increment": '1',
          "last_number": '0',
          "leading_zero": '5',
          "description": 'Nomor SGE untuk Nasabah'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Number Format') do
      tags 'Number Format'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
