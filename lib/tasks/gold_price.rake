namespace :gold_price do
  desc 'schedule mendapatkan harga emas harian'
  task update: :environment do
    GoldPriceService.new.execute
  end
end
