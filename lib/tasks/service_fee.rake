namespace :service_fee do
  desc 'schedule merubah status bila ada data yang belaku hari ini maka data dengan status true sebelumnya menjadi false'
  task update_status_by_date: :environment do
    ServiceFee.where(status: true).update(status: false)
    ServiceFee.where(valid_date: Date.today).update(status: true)
  end
end
