class SispusChannel < ApplicationCable::Channel
  def subscribed
    check_current_owner

    stream_from "sispus-#{@current_owner[:office_id]}-#{@current_owner[:role]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  private

  def check_current_owner
    response = RestClient.get "#{ENV['AUTH_URL']}/api/v1/me", { Authorization: "Bearer #{params[:token]}" }
    response = JSON.parse(response.body)['user']
    @current_owner = {
      id: response['_id']['$oid'],
      name: "#{response['first_name']} #{response['last_name']}",
      office_id: response['user_office']['officeable_id']['$oid'],
      office_type: response['user_office']['officeable_type'],
      role: response['user_roles']['role_name']
    }
  rescue RestClient::Unauthorized, RestClient::Forbidden
    unsubscribed
  end
end
