# Model
class Area
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  include Rails.application.routes.url_helpers

  field :name, type: String
  field :code, type: String
  field :city_id, type: String
  field :area_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :last_number_cif, type: Integer, default: 1
  field :last_number_sge, type: Integer, default: 1
  field :last_number_finance, type: Integer, default: 1
  field :last_number_work_cap, type: Integer, default: 1
  field :regional_id, type: String
  field :company_name, type: String

  validates_presence_of :name, :code, :regional_id
  validates_uniqueness_of :name, :code, case_sensitive: false, scope: :deleted_at

  has_many :office_hour_schedules, dependent: :restrict_with_exception
  has_many :branch_offices, dependent: :restrict_with_exception
  belongs_to :regional, inverse_of: :areas, foreign_key: 'regional_id', optional: true
  belongs_to :city
end
