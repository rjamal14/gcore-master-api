# Model
class ProductPeriodHistory
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :period_of_time, presence: true
  validates :due_date, presence: true
  validates :grace_period, presence: true
  validates :fine_amount, presence: true
  validates :fine_percentage, presence: true

  field :product_id, type: BSON::ObjectId
  field :province_id, type: BSON::ObjectId
  field :period_of_time, type: Integer
  field :due_date, type: Integer
  field :grace_period, type: Integer
  field :fine_amount, type: Float
  field :fine_percentage, type: Float

  belongs_to :province, optional: true, foreign_key: :province_id
  belongs_to :product, foreign_key: :product_id
end
