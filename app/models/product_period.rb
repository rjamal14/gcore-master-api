# Model
class ProductPeriod
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :period_of_time, presence: true
  validates :due_date, presence: true
  validates :grace_period, presence: true
  validates :auction_period, presence: true
  validates :fine_amount, presence: true
  validates :fine_percentage, presence: true

  field :product_id, type: BSON::ObjectId
  field :province_id, type: BSON::ObjectId
  field :period_of_time, type: Integer
  field :due_date, type: Integer
  field :auction_period, type: Integer
  field :grace_period, type: Integer
  field :fine_amount, type: Float
  field :fine_percentage, type: Float

  belongs_to :province, optional: true, foreign_key: :province_id
  belongs_to :product, foreign_key: :product_id

  before_update :history_record

  def history_record
    @prev_record = ProductPeriod.where(id: id).last
    return if @prev_record.blank?

    ProductPeriodHistory.new(
      product_id: @prev_record.product_id,
      province_id: @prev_record.province_id,
      period_of_time: @prev_record.period_of_time,
      due_date: @prev_record.due_date,
      grace_period: @prev_record.grace_period,
      fine_amount: @prev_record.fine_amount,
      fine_percentage: @prev_record.fine_percentage
    ).save
  end
end
