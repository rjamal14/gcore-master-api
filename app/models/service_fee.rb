# Slte Model
class ServiceFee
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  validates :valid_date, presence: true
  validates :value_fee, presence: true
  validates :status, presence: true
  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true

  validate :valid_date_cannot_be_in_the_past
  validate :value_fee_greater

  field :valid_date, type: Date
  field :value_fee, type: Float
  field :status, type: Boolean, default: true
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId

  def valid_date_cannot_be_in_the_past
    errors.add(:valid_date, 'Tanggal tidak Kurang dari hari ini') if valid_date.present? && valid_date < Date.today
  end

  def value_fee_greater
    errors.add(:value_slte, 'Tidak boleh kurang dari 0') if value_fee.present? && value_fee.negative?
  end
end
