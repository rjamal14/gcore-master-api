# Gold Price Model
class GoldPrice
  include Mongoid::Document
  include Mongoid::Timestamps::Short
  field :gram, type: Float
  field :kilogram, type: Float
  field :ounce, type: Float
end
