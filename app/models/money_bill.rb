class MoneyBill
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia

  field :nominal, type: Integer
  field :money_type
  field :status, type: Mongoid::Boolean, default: true
  field :description, type: String
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId

  enumerize :money_type, in: [:paper, :coin]

  validates_uniqueness_of :nominal, scope: [:deleted_at, :money_type]
end
