# Model
class ProductAdminCostDiscount
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :insurance_item_id, presence: true
  validates_presence_of :repawn_1, :repawn_2, :repawn_3, :repawn_4, :repawn_5, :repawn_6
  validates_presence_of :repawn_7, :repawn_8, :repawn_9, :repawn_10, :repawn_11, :repawn_12

  field :product_id, type: BSON::ObjectId
  field :insurance_item_id, type: String
  field :repawn_1, type: Float
  field :repawn_2, type: Float
  field :repawn_3, type: Float
  field :repawn_4, type: Float
  field :repawn_5, type: Float
  field :repawn_6, type: Float
  field :repawn_7, type: Float
  field :repawn_8, type: Float
  field :repawn_9, type: Float
  field :repawn_10, type: Float
  field :repawn_11, type: Float
  field :repawn_12, type: Float

  belongs_to :product, foreign_key: :product_id
  belongs_to :insurance_item, foreign_key: :insurance_item_id
end
