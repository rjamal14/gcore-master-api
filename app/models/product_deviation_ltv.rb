# Deviasi
class ProductDeviationLtv
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :product_id, type: BSON::ObjectId
  field :insurance_item_id, type: BSON::ObjectId
  field :regional_id, type: BSON::ObjectId
  field :branch, type: Float
  field :area, type: Float
  field :region, type: Float
  field :head, type: Float

  validates_presence_of :branch, :area, :region, :head, :insurance_item_id, :product_id, :regional_id, scope: :deleted_at
  validates_uniqueness_of :regional_id, scope: [:product_id, :insurance_item_id]
  before_create :ltv_validator

  belongs_to :product

  def ltv_value
    product.product_finances.find_by(insurance_item_id: insurance_item_id.to_s)
           .product_finance_items.find_by(regional_id: regional_id.to_s).ltv_value
  rescue StandardError
    0
  end

  def ltv_validator
    raise StandardError, 'Tidak Valid' unless ltv_value < branch && branch < area && area < region && region < head
  end
end
