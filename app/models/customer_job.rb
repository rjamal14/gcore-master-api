# Model
class CustomerJob
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :name, type: String
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  validates_presence_of :name
  validates_uniqueness_of :name, scope: :deleted_at
end
