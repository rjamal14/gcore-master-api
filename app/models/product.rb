# Model
class Product
  extend Enumerize

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  include Rails.application.routes.url_helpers

  validates :name, presence: true
  validates :status, presence: true
  validates :tiering_type, presence: true
  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at

  field :name, type: String
  field :status, type: Boolean, default: true
  field :product_image, type: String
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId
  field :tiering_type
  field :type, default: :regular
  field :one_obligor_value, type: Integer
  enumerize :type, in: [:installment, :regular, :option]
  enumerize :tiering_type, in: [:percentage, :nominal]

  validates_presence_of :type, :name

  mount_base64_uploader :product_image, ProductUploader, file_name: ->(u) { u.name }

  has_one :product_period, dependent: :destroy
  has_many :product_finances, dependent: :destroy
  has_many :product_period_histories, dependent: :destroy
  has_many :product_rental_costs, dependent: :destroy
  has_many :product_rental_cost_discounts, dependent: :destroy
  has_many :product_admin_costs, dependent: :destroy
  has_many :product_admin_cost_discounts, dependent: :destroy
  has_many :product_deviation_setups, dependent: :destroy
  has_many :product_deviation_items, dependent: :destroy
  has_many :product_one_obligors, dependent: :destroy
  has_many :product_insurance_items, dependent: :destroy
  has_many :product_deviation_ltv
  has_many :product_deviation_rentals
  has_many :product_deviation_admins
  has_many :product_deviation_insurance_items
  has_many :product_deviation_one_obligors
  has_many :product_prolongations

  accepts_nested_attributes_for :product_period, allow_destroy: true
  accepts_nested_attributes_for :product_finances, allow_destroy: true
  accepts_nested_attributes_for :product_admin_costs, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_setups, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_items, allow_destroy: true
  accepts_nested_attributes_for :product_admin_cost_discounts, allow_destroy: true
  accepts_nested_attributes_for :product_rental_cost_discounts, allow_destroy: true
  accepts_nested_attributes_for :product_rental_costs, allow_destroy: true
  accepts_nested_attributes_for :product_period_histories, allow_destroy: true
  accepts_nested_attributes_for :product_one_obligors, allow_destroy: true
  accepts_nested_attributes_for :product_insurance_items, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_ltv, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_rentals, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_admins, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_insurance_items, allow_destroy: true
  accepts_nested_attributes_for :product_deviation_one_obligors, allow_destroy: true
  accepts_nested_attributes_for :product_prolongations, allow_destroy: true
end
