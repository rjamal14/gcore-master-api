# Model
class ProductRentalCost
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :rental_cost_percentage_15_days, presence: true
  validates :insurance_item_id, presence: true

  field :product_id, type: BSON::ObjectId
  field :rental_cost_percentage_15_days, type: Float
  field :insurance_item_id, type: String
  field :rental_cost_percentage, type: Float
  field :regional_id, type: BSON::ObjectId

  validates_uniqueness_of :insurance_item_id, scope: [:product_id, :regional_id, :deleted_at]

  belongs_to :product, foreign_key: :product_id
  belongs_to :insurance_item, foreign_key: :insurance_item_id
  belongs_to :regional
end
