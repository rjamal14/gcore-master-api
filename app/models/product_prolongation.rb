class ProductProlongation
  extend Enumerize

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia

  scope :admin, -> { where(type: :admin) }
  scope :rental, -> { where(type: :rental) }

  field :product_id
  field :insurance_item_id
  field :order, type: Integer, default: 1
  field :type, type: String
  field :value, type: Float, default: 0

  enumerize :type, in: [:admin, :rental]

  belongs_to :product
  belongs_to :insurance_item

  validates_presence_of :product, :insurance_item, :order, :type, :value
  validates_uniqueness_of :order, scope: [:product_id, :insurance_item_id, :deleted_at, :type]
  # validate :validate_obj_before
  # validate :validate_order_before

  # def validate_obj_before
  #   return if order == 1
  #
  #   errors.add(:product_prolongation, 'Nilai gadai Ulang Selanjutnya Harus Lebih Besar') unless check_value_before
  # end
  #
  # def check_value_before
  #   value_before = query('value') || 100
  #   value_before < value || value_before.nil?
  # end
  #
  # def query(value, _added = '')
  #   eval("product.product_prolongations.#{type}.where(insurance_item_id: insurance_item_id#{_added}).last.try(:#{value})")
  # end
  #
  # def validate_order_before
  #   return if order == 1
  #
  #   added = ',order: order-1'
  #   errors.add(:product_prolongation, 'Gadai Ulang Sebelumnya harus Ada') unless query('order', added).present?
  # end
end
