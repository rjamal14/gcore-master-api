# City Model
class City
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :city_code, type: Integer
  field :name, type: String
  field :province_id, type: Integer
  field :created_by_id, type: String
  field :updated_by_id, type: String
  has_many :unit_offices, foreign_key: 'city_id', primary_key: 'id', dependent: :restrict_with_exception
  has_many :branch_offices, foreign_key: 'city_id', primary_key: 'id', dependent: :restrict_with_exception
  has_many :head_offices, dependent: :restrict_with_exception
  has_many :districts, primary_key: 'city_code', dependent: :restrict_with_exception
  belongs_to :province, foreign_key: 'province_id', primary_key: 'province_code'
  belongs_to :area, foreign_key: 'area_id', inverse_of: :cities, optional: true
  validates_presence_of :name, :province_id
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at
  validates_uniqueness_of :city_code, scope: :deleted_at

  validates_format_of :name, with: /^[a-z ]+$/i, multiline: true
  before_save :find_highest_city_code

  def find_highest_city_code
    return if city_code.present?

    self.city_code = City.max(:city_code) + 1
  end
end
