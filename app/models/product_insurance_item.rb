# ProductInsuranceItem
class ProductInsuranceItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :name, presence: true
  validates :insurance_item_id, presence: true
  validates :minimum_karatase, presence: true, inclusion: 1..24
  validates :certificate, presence: true
  validates_uniqueness_of :name, scope: [:product_id, :deleted_at]

  field :product_id, type: BSON::ObjectId
  field :name, type: String
  field :insurance_item_id, type: BSON::ObjectId
  field :minimum_karatase, type: Integer
  field :certificate, type: String

  belongs_to :insurance_item, foreign_key: :insurance_item_id
  belongs_to :product, foreign_key: :product_id
end
