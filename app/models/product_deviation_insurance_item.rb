# Deviasi
class ProductDeviationInsuranceItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :product_id, type: BSON::ObjectId
  field :insurance_item_id, type: BSON::ObjectId
  field :branch_weight, type: Float
  field :area_weight, type: Float
  field :region_weight, type: Float
  field :head_weight, type: Float

  field :branch_karats, type: Float
  field :area_karats, type: Float
  field :region_karats, type: Float
  field :head_karats, type: Float

  validates_presence_of :branch_karats, :branch_weight, :area_karats, :area_weight, :region_karats, :region_weight,
                        :head_weight, :head_karats
  validate :karats, :weight

  belongs_to :product

  def karats
    errors.add(:deviasi_karatase, 'Tidak Valid') unless branch_karats > area_karats && area_karats > region_karats && region_karats > head_karats
  end

  def weight
    errors.add(:deviasi_berat, 'Tidak Valid') unless branch_weight > area_weight && area_weight > region_weight && region_weight > head_weight
  end
end
