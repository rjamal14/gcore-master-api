# Bank Model
class Bank
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :name, type: String
  field :bank_code, type: String
  field :description, type: String
  field :status, type: Boolean, default: true
  field :created_by_id, type: String
  field :updated_by_id, type: String
  validates_presence_of :name, :bank_code
  validates_uniqueness_of :name, :bank_code, case_sensitive: false, scope: :deleted_at
end
