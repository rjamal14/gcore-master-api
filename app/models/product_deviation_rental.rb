# Deviasi
class ProductDeviationRental
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :product_id, type: BSON::ObjectId
  field :insurance_item_id, type: BSON::ObjectId
  field :regional_id, type: BSON::ObjectId
  field :branch, type: Float
  field :area, type: Float
  field :region, type: Float
  field :head, type: Float

  validates_presence_of :branch, :area, :region, :head, :regional_id, :insurance_item_id, :product_id
  validates_uniqueness_of :regional_id, scope: [:product_id, :insurance_item_id]
  before_create :rental_validator

  belongs_to :product

  def rental_value
    product.product_rental_costs.find_by(regional_id: regional_id.to_s, insurance_item_id: insurance_item_id).rental_cost_percentage
  rescue StandardError
    100
  end

  def rental_validator
    raise StandardError, 'Tidak Valid' unless rental_value > branch && branch > area && area > region && region > head
  end
end
