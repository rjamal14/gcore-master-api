# user
class User
  include Mongoid::Document
  field :id
  field :office_code
  field :area_code
  field :office_id
  field :office_type
  field :office_name
  field :last_number_cif
  field :last_number_sge
end
