class Urban
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :name, type: String
  field :district_id, type: Integer
  field :postal_code

  belongs_to :district, primary_key: 'district_code'

  validates_presence_of :name, :district_id, :postal_code
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at

  validates_format_of :name, with: /^[a-z]+$/i, multiline: true
  validates_format_of :postal_code, with: /^[0-9]+$/i, multiline: true
end
