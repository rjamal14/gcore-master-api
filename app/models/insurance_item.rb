# Model
class InsuranceItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :name, presence: true
  validates :status, presence: true
  validates :description, presence: true
  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at

  field :name, type: String
  field :status, type: Boolean, default: true
  field :description, type: String
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId

  has_many :insurance_item_types, dependent: :restrict_with_exception
  has_many :product_rental_costs, dependent: :restrict_with_exception
  has_many :product_rental_cost_discounts, dependent: :restrict_with_exception
  has_many :product_admin_costs, dependent: :restrict_with_exception
  has_many :product_admin_cost_discounts, dependent: :restrict_with_exception
  has_many :product_deviation_setups, dependent: :restrict_with_exception
  has_many :product_deviation_items, dependent: :restrict_with_exception
  has_many :gold_market_prices, dependent: :restrict_with_exception

  accepts_nested_attributes_for :insurance_item_types, allow_destroy: true
end
