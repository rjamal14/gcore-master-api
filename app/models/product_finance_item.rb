# Model
class ProductFinanceItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Attributes::Dynamic

  field :product_finance_id, type: String
  field :insurance_item_id, type: String
  field :stle_value, type: Float
  field :ltv_value, type: Float
  field :regional_id, type: BSON::ObjectId

  validates_presence_of :stle_value, :ltv_value, :regional_id

  belongs_to :product_finance
  belongs_to :product, foreign_key: 'product_id', optional: true
  belongs_to :insurance_item, foreign_key: 'insurance_item_id', optional: true
  belongs_to :regional
end
