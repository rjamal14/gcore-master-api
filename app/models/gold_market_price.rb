# GoldMarketPrice
class GoldMarketPrice
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  validates :price, presence: true
  validates :valid_date, presence: true
  validates :insurance_item_id, presence: true
  validate :value_price

  field :price, type: Float
  field :valid_date, type: Date
  field :insurance_item_id, type: BSON::ObjectId

  belongs_to :insurance_item, foreign_key: :insurance_item_id

  def value_price
    errors.add(:price, 'Tidak boleh kurang dari 0') if price.present? && price.negative?
  end
end
