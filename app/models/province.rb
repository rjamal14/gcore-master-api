# Model Province
class Province
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :province_code, type: Integer
  field :name, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :country_id, type: String
  has_many :cities, primary_key: 'province_code', dependent: :restrict_with_exception
  belongs_to :country, foreign_key: 'country_id', inverse_of: :provinces, optional: true
  validates_presence_of :name, :country_id
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at
  validates_uniqueness_of :province_code, scope: :deleted_at

  validates_format_of :name, with: /^[a-z ]+$/i, multiline: true

  has_many :product_periods
  has_many :product_period_historys

  before_save :find_highest_province_code

  def find_highest_province_code
    return if province_code.present?

    self.province_code = Province.max(:province_code) + 1
  end
end
