# Model
class NumberFormat
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :name, type: String
  field :year, type: Integer
  field :code, type: String
  field :number_increment, type: Integer, default: 1
  field :last_number, type: Integer, default: 0
  field :num_format, type: String
  field :leading_Zero, type: Integer, default: 5
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  validates_presence_of :name, :year, :code, :num_format, :number_increment, :leading_Zero, :last_number
  validates_uniqueness_of :name, :num_format
end
