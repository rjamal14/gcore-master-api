# ProductOneObligor
class ProductOneObligor
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  after_initialize :init
  validates :product_id, presence: true
  validates :limit_name, presence: true
  validates :limit_item, presence: true
  validates :one_obligor_value, presence: true
  validates_uniqueness_of :product_id, scope: :limit_name

  field :product_id, type: BSON::ObjectId
  field :limit_name, type: String
  field :limit_item, type: Array
  field :one_obligor_value, type: Float
  belongs_to :product

  def init
    self.one_obligor_value = 0.0
    limit_item.each { |k| self.one_obligor_value += k['item_value'].to_f }
  end
end
