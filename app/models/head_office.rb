# HeadOffice Model
class HeadOffice
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :name, type: String
  field :code, type: String
  field :city_id, type: String
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal
  field :last_number_cif, type: Integer, default: 1
  field :last_number_sge, type: Integer, default: 1
  field :last_number_finance, type: Integer, default: 1
  field :last_number_work_cap, type: Integer, default: 1

  has_many :regionals
  belongs_to :city, optional: true
  validates_presence_of :name, :code
  validates_uniqueness_of :name, :code, case_sensitive: false, scope: :deleted_at
end
