# Verification Type Model
class VerificationType
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Validatable
  include ActiveModel::Validations
  include Validators

  field :name, type: String
  field :status, type: Mongoid::Boolean
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :status
  validates_uniqueness_of :name

  has_and_belongs_to_many :verification_items, inverse_of: :verification_types, foreign_key: 'verification_item_ids'
end
