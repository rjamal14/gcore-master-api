# Model
class ProductAdminCost
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :min_loan_range, presence: true
  validates :max_loan_range, presence: true
  validates :cost_nominal, presence: true
  validates :cost_percentage, presence: true
  validates :insurance_item_id, presence: true

  validate :valid_loan_range

  field :product_id, type: BSON::ObjectId
  field :min_loan_range, type: Float
  field :max_loan_range, type: Float
  field :cost_nominal, type: Integer
  field :cost_percentage, type: Float
  field :insurance_item_id, type: String

  belongs_to :product, foreign_key: :product_id
  belongs_to :insurance_item, foreign_key: :insurance_item_id

  def valid_loan_range
    errors.add(:min_loan_range, 'Tidakboleh lebih dari max_loan_range') if min_loan_range.present? && min_loan_range > max_loan_range
  end
end
