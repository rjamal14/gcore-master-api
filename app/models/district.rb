# District Model
class District
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :name, type: String
  field :district_code, type: Integer
  field :id, type: Integer
  field :city_id, type: Integer

  belongs_to :city, primary_key: 'city_code'
  has_many :urbans

  validates_presence_of :name, :city_id
  validates_uniqueness_of :name, case_sensitive: false, scope: :deleted_at
  validates_uniqueness_of :district_code
  before_save :find_highest_district_code

  def find_highest_district_code
    return if city_code.present?

    self.district_code = District.max(:district_code) + 1
  end

  validates_format_of :name, with: /^[a-z ]+$/i, multiline: true
end
