# InsuranceItemType
class InsuranceItemType
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  validates :name, presence: true

  field :name, type: String
  field :insurance_item_id, type: String

  belongs_to :insurance_item
end
