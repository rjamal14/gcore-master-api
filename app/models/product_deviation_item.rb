# Model
class ProductDeviationItem
  extend Enumerize

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :insurance_item_id, presence: true
  validates_uniqueness_of :product_id, scope: [:name, :deleted_at]
  validate :validate_deviation_items

  field :product_id, type: BSON::ObjectId
  field :name
  field :insurance_item_id, type: String
  field :branch, type: Hash
  field :area, type: Hash
  field :region, type: Hash
  field :headquarters, type: Hash

  enumerize :name, in: [:admin, :barang_jaminan]

  belongs_to :product, foreign_key: :product_id
  belongs_to :insurance_item, foreign_key: :insurance_item_id

  private

  def validate_deviation_items
    err_name = [area, region, headquarters]
    if name == 'barang_jaminan'
      err_unless(branch, 'berat', 'karatase')
      err_name.map { |a| err_true(a, 'berat', 'karatase') }
    else
      err_name.push(branch)
      err_name.map { |b| err_true(b, 'presentase', 'nominal') }
    end
  end

  def err_true(err, name1, name2)
    errors.add(err.to_sym, :blank) if err[name1].nil? || err[name2].nil?
  end

  def err_unless(err, name1, name2)
    errors.add(err.to_sym, :blank) unless err[name1].nil? || err[name2].nil?
  end
end
