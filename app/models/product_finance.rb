# Model
class ProductFinance
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Attributes::Dynamic

  field :product_id, type: String
  field :insurance_item_id, type: String
  field :min_karatase, type: Integer
  field :min_weight, type: Float
  field :min_loan, type: Integer
  field :max_loan, type: Integer

  validates_presence_of :min_karatase, :min_loan, :max_loan
  validates_uniqueness_of :insurance_item_id, scope: [:product_id, :deleted_at]
  validates :min_karatase, inclusion: 1..24
  validates :min_loan, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :max_loan, numericality: { greater_than_or_equal_to: 0, only_integer: true }

  has_many :product_finance_items

  accepts_nested_attributes_for :product_finance_items

  belongs_to :insurance_item
  belongs_to :product
end
