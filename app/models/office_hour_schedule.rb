# OfficeHourSchedule
class OfficeHourSchedule
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations
  include Mongoid::Paranoia

  validates :area_id, presence: true
  validates :schedule_date, presence: true
  validates :started_at, presence: true
  validates :closed_at, presence: true
  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true
  validate :schedule_date_cannot_be_in_the_past
  validates_uniqueness_of :area_id, scope: :schedule_date

  field :area_id, type: BSON::ObjectId
  field :schedule_date, type: Date
  field :started_at, type: Time
  field :closed_at, type: Time
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId

  belongs_to :area, foreign_key: :area_id

  def schedule_date_cannot_be_in_the_past
    errors.add(:schedule_date, 'Tanggal tidak Kurang dari hari ini') if schedule_date.present? && schedule_date < Date.today
  end
end
