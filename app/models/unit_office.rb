# Model Kantor Unit
class UnitOffice
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations
  field :name, type: String
  field :code, type: String
  field :city_id, type: String
  field :branch_office_id, type: String
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal
  field :last_number_cif, type: Integer, default: 1
  field :last_number_sge, type: Integer, default: 1
  field :last_number_ref, type: Integer, default: 1
  field :last_number_finance, type: Integer, default: 1
  field :last_number_work_cap, type: Integer, default: 1
  belongs_to :city, foreign_key: 'city_id', primary_key: 'id', optional: true
  belongs_to :branch_office, foreign_key: 'branch_office_id', primary_key: 'id', optional: true
  validates_presence_of :name, :code, :branch_office_id
  validates_uniqueness_of :name, scope: [:city_id, :deleted_at], case_sensitive: false
end
