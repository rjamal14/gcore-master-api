# Controller
class ProductDeviationSetup
  extend Enumerize

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :product_id, presence: true
  validates :name, presence: true
  validates :insurance_item_id, presence: true
  validates :branch, presence: true
  validates :area, presence: true
  validates :region, presence: true
  validates :headquarters, presence: true
  validates_uniqueness_of :product_id, scope: :name

  field :product_id, type: String
  field :name
  field :insurance_item_id, type: String
  field :branch, type: Float
  field :area, type: Float
  field :region, type: Float
  field :headquarters, type: Float

  validates_uniqueness_of :insurance_item_id, scope: :product_id

  enumerize :name, in: [:ltv, :sewa, :one_obligor]

  belongs_to :product, foreign_key: :product_id
  belongs_to :insurance_item, foreign_key: :insurance_item_id
end
