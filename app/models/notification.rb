class Notification
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps

  before_save :broadcast_notification

  field :type, type: String
  field :subject
  field :offices, type: Array
  field :body, type: String
  field :url, type: String
  field :is_read, type: Boolean, default: false

  scope :unread, -> { where(is_read: false) }
  enumerize :subject, in: [:transaction_approval, :cash_flow, :cashier]

  def broadcast_notification
    if type == 'siscab' || type == 'sispus'
      offices.each do |office|
        room = "#{type}-#{office['office_id']}-#{office['role']}"
        ActionCable.server.broadcast(room, notification_hash)
      end
    else
      ActionCable.server.broadcast(type, notification_hash)
    end
  end

  def notification_hash
    {
      subject: subject,
      type: type,
      body: body,
      url: url
    }
  end
end
