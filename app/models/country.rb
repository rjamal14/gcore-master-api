class Country
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  # before_destroy :check_for_child
  field :name, type: String
  field :nationality, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  has_many :provinces, dependent: :restrict_with_exception
  validates_presence_of :name, :nationality
  validates_uniqueness_of :name, :nationality, case_sensitive: false, scope: :deleted_at

  validates_format_of :name, :nationality, with: /^[a-z ]+$/i, multiline: true
end
