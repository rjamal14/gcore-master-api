# Verfication Item Model
class VerificationItem
  extend Enumerize

  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :status, type: Mongoid::Boolean
  field :document_needed
  enumerize :document_needed, in: [:mandatory, :not_needed, :optional]
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :status, :document_needed
  validates_uniqueness_of :name

  has_and_belongs_to_many :verification_types, inverse_of: :verification_types, foreign_key: 'verification_type_ids'
end
