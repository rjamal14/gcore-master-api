# Deviasi
class ProductDeviationAdmin
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :product_id, type: BSON::ObjectId
  field :insurance_item_id, type: BSON::ObjectId

  field :branch, type: Integer
  field :area, type: Integer
  field :region, type: Integer
  field :head, type: Integer

  before_create :admin_validator

  belongs_to :product

  def admin_validator
    raise StandardError, 'Tidak Valid' unless branch < area && area < region && region < head
  end
end
