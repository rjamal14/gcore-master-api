# Model Regional
class Regional
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  field :name, type: String
  field :company_name, type: String
  field :code, type: String
  field :head_office_id, type: String
  field :city_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :last_number_cif, type: Integer, default: 1
  field :last_number_sge, type: Integer, default: 1
  field :last_number_finance, type: Integer, default: 1
  field :last_number_work_cap, type: Integer, default: 1
  field :logo, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :code
  validates_uniqueness_of :code, :name, scope: :deleted_at

  mount_base64_uploader :logo, RegionalUploader, file_name: ->(u) { u.name }

  belongs_to :city, optional: true
  belongs_to :head_office, foreign_key: 'head_office_id', optional: true

  has_many :areas, foreign_key: 'regional_id', dependent: :restrict_with_exception
  has_many :product_finance_items, dependent: :restrict_with_exception
  has_many :product_rental_costs, dependent: :restrict_with_exception
end
