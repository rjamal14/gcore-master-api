# Serializer
class VerificationTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :status, :verification_items
end
