# Serializer
class InsuranceItemProductSerializer < ActiveModel::Serializer
  attributes :insurance_items
  def insurance_items
    object.product_finances.map do |admin|
      { id: admin.insurance_item.id || nil,
        name: admin.insurance_item.name || nil }
    rescue StandardError
      { id: nil,
        name: nil }
    end
  end
end
