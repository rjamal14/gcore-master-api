# Serializer
class ProductFinanceItemSerializer < ActiveModel::Serializer
  attributes :id, :product_finance_id, :stle_value, :ltv_value, :regional_id, :regional_name

  def regional_name
    object.try(:regional).try(:name)
  end
end
