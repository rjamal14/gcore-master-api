class DistrictSerializer < ActiveModel::Serializer
  attributes :id, :name, :city_name, :province_name, :district_code

  def city_name
    object.try(:city).try(:name)
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end
end
