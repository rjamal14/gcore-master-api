# Serializer
class ProductDeviationItemSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :name, :insurance_item_id, :branch, :area, :region, :headquarters, :insurance_item_name

  def insurance_item_name
    object.try(:insurance_item).try(:name)
  end
end
