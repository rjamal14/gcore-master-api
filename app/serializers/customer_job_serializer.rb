# Serializer
class CustomerJobSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :created_by_id, :updated_by_id, :created_at, :updated_at
end
