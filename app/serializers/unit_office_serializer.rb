# UnitOfficeSerializer
class UnitOfficeSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :description, :address, :province_name, :city_name,
             :branch_office_name

  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def city_name
    object.try(:city).try(:name)
  end

  def branch_office_name
    object.try(:branch_office).try(:name)
  end
end
