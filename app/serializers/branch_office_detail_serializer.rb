# BranchOfficeSerializer
class BranchOfficeDetailSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :description, :address, :latitude, :longitude, :province_name, :province_id, :city_name, :city_id,
             :area_office_name, :area_office_id, :last_number_cif, :last_number_sge
  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def province_id
    object.try(:city).try(:province).try(:id).to_s
  end

  def city_name
    object.try(:city).try(:name)
  end

  def city_id
    object.try(:city).try(:id).to_s
  end

  def area_office_name
    object.try(:area).try(:name)
  end

  def area_office_id
    object.try(:area).try(:id).to_s
  end
end
