class OfficesSerializer < ActiveModel::Serializer
  attributes :data
  def data
    case serialization_options[:office_type]
    when 'unit'
      unit_hash
    when 'branch'
      branch_hash
    when 'area'
      area_hash
    when 'region'
      region_hash
    else
      head_hash
    end
  end

  def unit_hash
    {
      office_name: object.name,
      branch: object.branch_office_id,
      branch_name: object.try(:branch_office).try(:name),
      branch_code: object.try(:branch_office).try(:code),
      area: object.try(:branch_office).try(:area_id),
      area_name: object.try(:branch_office).try(:area).try(:name),
      area_code: object.try(:branch_office).try(:area).try(:code),
      region: object.try(:branch_office).try(:area).try(:regional_id),
      region_name: object.try(:branch_office).try(:area).try(:regional).try(:name),
      region_code: object.try(:branch_office).try(:area).try(:regional).try(:code),
      head: object.try(:branch_office).try(:area).try(:regional).try(:head_office_id),
      head_name: object.try(:branch_office).try(:area).try(:regional).try(:head_office).try(:name)
    }
  end

  def branch_hash
    {
      office_name: object.name,
      branch: object.id,
      branch_name: object.try(:name),
      branch_code: object.try(:code),
      area: object.try(:area_id),
      area_name: object.try(:area).try(:name),
      area_code: object.try(:area).try(:code),
      region: object.try(:area).try(:regional_id),
      region_name: object.try(:area).try(:regional).try(:name),
      region_code: object.try(:area).try(:regional).try(:code),
      head: object.try(:area).try(:regional).try(:head_office_id),
      head_name: object.try(:area).try(:regional).try(:head_office).try(:name)
    }
  end

  def area_hash
    {
      office_name: object.name,
      area: object.try(:id),
      area_name: object.try(:name),
      area_code: object.try(:code),
      region: object.try(:regional_id),
      region_name: object.try(:regional).try(:name),
      region_code: object.try(:regional).try(:code),
      head: object.try(:regional).try(:head_office_id),
      head_name: object.try(:regional).try(:head_office).try(:name)
    }
  end

  def region_hash
    {
      office_name: object.name,
      region: object.try(:id),
      region_name: object.try(:name),
      region_code: object.try(:code),
      head: object.try(:head_office_id),
      head_name: object.try(:head_office).try(:name)
    }
  end

  def head_hash
    {
      office_name: object.name,
      head: object.try(:id),
      head_name: object.try(:name)
    }
  end
end
