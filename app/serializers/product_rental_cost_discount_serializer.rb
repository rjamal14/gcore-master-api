# Serializer
class ProductRentalCostDiscountSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :insurance_item_id, :repawn_1, :repawn_2, :repawn_3,
             :repawn_4, :repawn_5, :repawn_6,
             :repawn_7, :repawn_8, :repawn_9,
             :repawn_10, :repawn_11, :repawn_12
end
