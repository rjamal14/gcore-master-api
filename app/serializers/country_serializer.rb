# CountrySerializer
class CountrySerializer < ActiveModel::Serializer
  attributes :id, :name, :nationality
end
