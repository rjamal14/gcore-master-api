# SlteSerializer
class OfficeHourSchedulesSerializer < ActiveModel::Serializer
  attributes :area_id, :schedule_date, :started_at, :closed_at, :created_by_id, :updated_by_id
end
