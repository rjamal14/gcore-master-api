class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :type, :subject, :body, :url, :is_read

  def id
    object.try(:id).to_s
  end
end
