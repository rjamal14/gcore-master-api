class MoneyBillSerializer < ActiveModel::Serializer
  attributes :id, :nominal, :money_type, :status, :description

  def id
    object.try(:id).to_s
  end
end
