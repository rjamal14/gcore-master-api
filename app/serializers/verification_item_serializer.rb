# Serializer
class VerificationItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :status, :document_needed
end
