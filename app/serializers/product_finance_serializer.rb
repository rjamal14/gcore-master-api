# Serializer
class ProductFinanceSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :min_karatase, :min_loan, :max_loan, :min_weight
  has_one :insurance_item, only: [:id, :name]
  has_many :product_finance_items
end
