# Serializer
class ProductPeriodSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :province_id, :period_of_time, :due_date, :auction_period, :grace_period, :fine_amount, :fine_percentage
end
