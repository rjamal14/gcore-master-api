# GoldMarketPriceSerializer
class GoldMarketPriceSerializer < ActiveModel::Serializer
  attributes :id, :price, :valid_date, :insurance_item_id
end
