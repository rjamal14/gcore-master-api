# ProductInsuranceItemSerializer
class ProductInsuranceItemSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :name, :minimum_karatase, :certificate
  has_one :insurance_item, only: [:id, :name]
end
