class UrbanSerializer < ActiveModel::Serializer
  attributes :id, :name, :postal_code, :district_name, :city_name, :province_name

  def district_name
    object.try(:district).try(:name)
  end

  def city_name
    object.try(:district).try(:city).try(:name)
  end

  def province_name
    object.try(:district).try(:city).try(:province).try(:name)
  end
end
