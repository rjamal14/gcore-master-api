# ProvinceSerializer
class ProvinceSerializer < ActiveModel::Serializer
  attributes :id, :_id, :name, :country_name, :province_code, :country_id

  def country_name
    object.try(:country).try(:name)
  end

  def id
    object.id.to_s
  end

  def _id
    object.id
  end
end
