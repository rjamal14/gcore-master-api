# Serializer
class InsuranceItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :status, :total, :created_by_id, :updated_by_id

  has_many :insurance_item_types
end
