# ProductOneObligorSerializer
class ProductOneObligorSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :limit_name, :limit_item, :one_obligor_value
end
