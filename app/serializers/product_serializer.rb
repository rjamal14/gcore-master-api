# Serializer
class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :status, :tiering_type, :insurance_item_type, :period_of_time, :type
  def period_of_time
    object.product_period.period_of_time
  rescue StandardError
    nil
  end

  def insurance_item_type
    object.product_finances.map do |admin|
      { name: admin.insurance_item.name || nil }
    rescue StandardError
      { name: nil }
    end
  end
end
