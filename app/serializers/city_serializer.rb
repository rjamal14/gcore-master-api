# CitySerializer
class CitySerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :province_name, :country_name, :city_code, :province_id

  def province_id
    object.try(:province).try(:id).to_s
  end

  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:province).try(:name)
  end

  def country_name
    object.try(:province).try(:country).try(:name)
  end
end
