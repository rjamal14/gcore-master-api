# Serializer
class AreaSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :address, :province_name, :city_name,
             :region_office_name
  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def city_name
    object.try(:city).try(:name)
  end

  def region_office_name
    object.try(:regional).try(:name)
  end
end
