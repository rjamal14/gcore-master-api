class ProductAutocompleteSerializer < ActiveModel::Serializer
  attributes :id, :name
end
