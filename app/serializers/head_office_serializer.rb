# Head Office Serializer
class HeadOfficeSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :description, :address, :province_name, :city_name
  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def city_name
    object.try(:city).try(:name)
  end
end
