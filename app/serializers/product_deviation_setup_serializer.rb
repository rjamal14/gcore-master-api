# Serializer
class ProductDeviationSetupSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :insurance_item_id, :name, :branch, :area, :region, :headquarters
end
