# Serializer
class AreaDetailSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :description, :address, :latitude, :longitude, :province_name, :province_id, :city_name, :city_id,
             :region_office_name, :region_office_id, :company_name
  def id
    object.try(:id).to_s
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def province_id
    object.try(:city).try(:province).try(:id).to_s
  end

  def city_name
    object.try(:city).try(:name)
  end

  def city_id
    object.try(:city).try(:id).to_s
  end

  def region_office_name
    object.try(:regional).try(:name)
  end

  def region_office_id
    object.try(:regional).try(:id).to_s
  end
end
