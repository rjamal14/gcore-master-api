# Serializer
class ProductDetailSerializer < ActiveModel::Serializer
  attributes :id, :name, :type, :status, :product_image, :tiering_type, :one_obligor_value
  has_one :product_period
  has_many :product_insurance_items, only: [:id, :name, :insurance_item_id, :minimum_karatase, :certificate]
  has_many :product_finances, :product_insurance_items, :product_admin_costs,
           :product_admin_cost_discounts, :product_rental_costs, :product_rental_cost_discounts,
           :product_one_obligors, :product_deviation_ltv, :product_deviation_admins, :product_deviation_rentals,
           :product_deviation_insurance_items, :product_deviation_insurance_items, :product_prolongations
  has_many :product_deviation_one_obligors
end
