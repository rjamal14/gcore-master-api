class ShowProvinceSerializer < ActiveModel::Serializer
  attributes :id, :name, :province_code, :country_id, :created_at,
             :updated_at, :created_by_id, :updated_by_id

  def country_id
    {
      id: object.country.id,
      name: object.country.try(:name)
    }
  end
end
