class RegionOfficeDetailSerializer < ActiveModel::Serializer
  attributes :_id, :id, :name, :code, :description, :address, :latitude, :longitude, :province_name, :province_id, :city_name, :city_id, :logo, :company_name,
             :head_office_name, :head_office_id
  def id
    object.try(:id).to_s
  end

  def logo
    object.try(:logo).try(:url)
  end

  def province_name
    object.try(:city).try(:province).try(:name)
  end

  def province_id
    object.try(:city).try(:province).try(:id).to_s
  end

  def city_name
    object.try(:city).try(:name)
  end

  def city_id
    object.try(:city).try(:id).to_s
  end

  def head_office_name
    object.try(:head_office).try(:name)
  end

  def head_office_id
    object.try(:head_office).try(:id).to_s
  end
end
