# Serializer
class NumberFormatSerializer < ActiveModel::Serializer
  attributes :id, :name, :year, :code, :num_format
end
