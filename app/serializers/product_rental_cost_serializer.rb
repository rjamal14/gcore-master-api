# Serializer
class ProductRentalCostSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :rental_cost_percentage, :rental_cost_percentage_15_days, :regional_id, :regional_name
  has_one :insurance_item, only: [:id, :name]

  def regional_name
    object.try(:regional).try(:name)
  end
end
