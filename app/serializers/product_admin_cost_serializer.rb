# Serializer
class ProductAdminCostSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :insurance_item_id, :min_loan_range, :insurance_item_name,
             :max_loan_range, :cost_nominal, :cost_percentage

  def insurance_item_name
    object.try(:insurance_item).try(:name)
  end
end
