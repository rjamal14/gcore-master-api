module Publishers
  class CreateNotificationPublisher
    QUEUE_NAME = 'notification.create.job'.freeze

    def initialize(notifications)
      @notifications = notifications
    end

    def publish(options = {})
      channel = ::Publisher::BunnyPublisher.connection.create_channel

      exchange = channel.exchange(
        ENV['AMQP_EXCHANGE'],
        type: 'direct',
        durable: true
      )

      headers = { 'x-delay' => options[:delay_time].to_i * 1_000 } if options[:delay_time].present?
      exchange.publish(@notifications.to_json, routing_key: QUEUE_NAME, headers: headers)
    end
  end
end
