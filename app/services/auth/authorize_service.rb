# Autorize Service

module Auth
  # Autorize Service
  class AuthorizeService
    def initialize
      @client = OAuth2::Client.new(
        ENV['CLIENT_ID'],
        ENV['CLIENT_SECRET'],
        site: ENV['AUTH_URL'],
        logger: Logger.new("log/auth-#{Time.now.strftime('%d-%m-%Y')}.log", 'daily')
      )

      @token_barier = "Bearer #{credentials.token}"
    end

    def credentials
      @client.client_credentials.get_token
    end

    def password(username, password)
      @client.password.get_token(username, password)
    end

    def auth_code(code)
      @client.auth_code.get_token(code, redirect_uri: 'urn:ietf:wg:oauth:2.0:oob')
    end

    def token
      credentials
    end

    def token_setter(token_barier = '')
      @token_barier = if token_barier.present?
                        token_barier
                      else
                        "Bearer #{credentials.token}"
                      end
    end

    def current_user
      response = RestClient.get "#{ENV['AUTH_URL']}/oauth/token/info", { Authorization: @token_barier }
    rescue RestClient::Unauthorized, RestClient::Forbidden, RestClient::ImATeapot, RestClient::NotFound
      nil
    else
      data = JSON.parse(response.body)

      if data['resource_owner_type'] == 'User'
        assign_user(data['user'])
      else
        'application'
      end
    end

    def assign_user(data_user)
      data_user['id'] = data_user['_id']['$oid']
      data_user.delete('_id')

      assign_office(data_user)

      JSON.parse(data_user.to_json, object_class: User)
    end

    def assign_office(data_user)
      data_user['office_id'] = data_user['user_office']['officeable_id']['$oid'].to_s
      data_user['office_type'] = data_user['user_office']['officeable_type']
      data_user['office_name'] = generate_office(data_user['office_type'], data_user['office_id']).name
      data_user['office_code'] = generate_office(data_user['office_type'], data_user['office_id']).code
      data_user['last_number_sge'] = generate_office(data_user['office_type'], data_user['office_id']).last_number_sge
      data_user['last_number_cif'] = generate_office(data_user['office_type'], data_user['office_id']).last_number_cif
      data_user['area_code'] = check_area_code(data_user['office_type'], data_user['office_id'])
      data_user.delete('user_office')
    end

    def generate_office(type, id)
      case type
      when 'branch'
        BranchOffice.find(id)
      when 'head'
        HeadOffice.find(id)
      when 'unit'
        UnitOffice.find(id)
      when 'area'
        Area.find(id)
      else
        Regional.find(id)
      end
    end

    def check_area_code(type, id)
      case type
      when 'branch'
        area_id = BranchOffice.find(id).area_id
        area_code = Area.find(area_id).code
      when 'unit'
        branch_id = UnitOffice.find(id).branch_office_id
        area_id = BranchOffice.find(branch_id).area_id
        area_code = Area.find(area_id).code
      else
        area_code = nil
      end
      area_code
    end

    def get_transaction(url, params = '', _data = '')
      response = RestClient.get "#{ENV['API_URL_TRANSACTION']}/api/v1/#{url}?#{params}", { Authorization: @token_barier }
      JSON.parse(response.body)
    end
  end
end
