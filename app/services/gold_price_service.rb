# Gold Price Service
class GoldPriceService
  def initialize
    @doc = Nokogiri::HTML(open('https://harga-emas.org/')).css('tr td')
    @kilogram = @doc[55].content.gsub('.', '').gsub(',', '.').to_f
    @gram = @doc[48].content.gsub('.', '').gsub(',', '.').to_f
  end

  def body
    @ounce = @doc[41].content.gsub('.', '').gsub(',', '.').to_f
  end

  def execute
    body
    @gold_price = GoldPrice.create(kilogram: @kilogram,
                                   gram: @gram, ounce: @ounce)
  end
end
