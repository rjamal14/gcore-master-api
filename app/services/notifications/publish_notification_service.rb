module Notifications
  class PublishNotificationService
    def initialize(msg:)
      @notification_hash = {
        type: msg['type'],
        offices: msg['offices'],
        subject: msg['subject'],
        body: msg['body'],
        url: msg['url']
      }
    end

    def save_notification
      Notification.new(@notification_hash)
    end

    def execute
      'failed' unless save_notification.save
    end
  end
end
