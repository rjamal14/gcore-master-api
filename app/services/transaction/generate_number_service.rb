# Generate Number Service
module Transaction
  # Generate Number Service
  class GenerateNumberService
    def initialize(office_id, resource, office_type)
      @resource = {
        office_id: office_id,
        office_type: office_type,
        format_type: resource,
        date_str: Date.today.to_s.tr('-', '')
      }
      set_office_type
      check_region_code
      set_office_code
      @office_code = "#{@region_code}-#{@office_code}-"
    end

    def set_office_code
      @office_code = if @resource[:office_type] == 'unit'
                       @office.code.to_s.rjust(3, '0')
                     else
                       @office.code.to_s.rjust(2, '0')
                     end
    end

    def check_region_code
      @region_code = if @resource[:office_type] == 'unit'
                       @office.branch_office.area.code
                     else
                       @office.area.code
                     end
    end

    def set_office_type
      @office = case @resource[:office_type]
                when 'unit'
                  UnitOffice.find(@resource[:office_id])
                when 'branch'
                  BranchOffice.find(@resource[:office_id])
                when 'head'
                  HeadOffice.find(@resource[:office_id])
                when 'area'
                  Area.find(@resource[:office_id])
                when 'regional'
                  Regional.find(@resource[:office_id])
                end
    end

    def generate_sge
      @number_generated = "GC-#{@office.code}-#{@resource[:date_str]}-#{@last_number}"
    end

    def generate_cif
      @number_generated = "#{@office_code}#{@last_number}"
    end

    def generate_budget_code
      @number_generated = "FR-#{@office.code}-#{@resource[:date_str]}-#{@last_number}"
    end

    def generate_reference_number
      type_of_cash_code = @resource[:format_type] == 'ref-cash' ? '01' : '02'
      @number_generated = "#{@office.code}-#{type_of_cash_code}-#{@resource[:date_str]}-#{@last_number}"
    end

    def generate_working_capital_number
      @number_generated = "#{@office.code}-#{@resource[:date_str]}-#{@last_number}"
    end

    def execute_sge_number
      number_format = NumberFormat.find_by(code: 'SGE')
      @last_number = @office.last_number_sge.to_s.rjust(number_format.leading_Zero, '0')
      generate_sge
      @office.update_attribute(:last_number_sge, @office.last_number_sge += 1)
    end

    def execute_cif_number
      number_format = NumberFormat.find_by(code: 'CIF')
      @last_number = @office.last_number_cif.to_s.rjust(number_format.leading_Zero, '0')
      generate_cif
      @office.update_attribute(:last_number_cif, @office.last_number_cif += 1)
    end

    def execute_ref_code
      @last_number = @office.last_number_ref.to_s.rjust(5, '0')
      generate_reference_number
      @office.update_attribute(:last_number_finance, @office.last_number_ref += 1)
    end

    def execute_finance_code
      number_format = NumberFormat.find_by(code: 'CIF')
      @last_number = @office.last_number_finance.to_s.rjust(number_format.leading_Zero, '0')
      generate_budget_code
      @office.update_attribute(:last_number_finance, @office.last_number_finance += 1)
    end

    def execute_working_capital
      @last_number = @office.last_number_work_cap.to_s.rjust(5, '0')
      generate_working_capital_number
      @office.update_attribute(:last_number_work_cap, @office.last_number_work_cap += 1)
    end

    def execute
      case @resource[:format_type]
      when 'SGE', 'sge'
        execute_sge_number
      when 'cif', 'CIF'
        execute_cif_number
      when 'ref-petty', 'ref-cash'
        execute_ref_code
      when 'work-cap', 'working-capital'
        execute_working_capital
      else
        execute_finance_code
      end
      @number_generated
    end
  end
end
