module Transaction
  class SimulateInstallmentService < InstallmentAttributeService
    def find_admin_cost(insurance_item_id, loan_amount)
      admin = @product.product_admin_costs.where(:max_loan_range.gte => loan_amount, :min_loan_range.lte => loan_amount)
      admin = admin.find_by(insurance_item_id: insurance_item_id)
      @product.tiering_type == 'percentage' ? loan_amount * admin[:cost_percentage] / 100 : admin[:cost_nominal]
    end

    def simulation_hash
      {
        loan_amount: @loan_amount,
        tenor: @tenor,
        installment_before_interest: (@loan_amount / @tenor).to_i,
        monthly_installment: (@loan_amount / @tenor + @loan_amount * @rental_cost / 100).to_i,
        admin_fee: find_admin_cost(@data[:insurance_item_id], @loan_amount).to_i,
        down_payment: @down_payment,
        monthly_fee: @loan_amount * @rental_cost / 100,
        total_down_payment: @down_payment + find_admin_cost(@data[:insurance_item_id], @loan_amount).to_i,
        due_date_simulation: @due_date_simulation
      }
    end

    def due_date_simulation
      due_date = Date.today
      @due_date_simulation = []
      @tenor.times do |i|
        due_date += 30.days
        @due_date_simulation.push({
                                    installment_order: i + 1,
                                    due_date: due_date
                                  })
      end
    end

    def execute(tenor:, down_payment:)
      return false if down_payment.to_i < (@buy_price - @financing_amount)

      @loan_amount = @buy_price - down_payment.to_i
      @tenor = tenor
      @down_payment = down_payment.to_i
      due_date_simulation
      simulation_hash
    end
  end
end
