module Transaction
  # Deviation Service
  class DeviationService
    def initialize(product_id, insurance_item_id, regional_id, loan_amount)
      @product_id = product_id
      @insurance_item_id = insurance_item_id
      @regional_id = regional_id
      @loan_amount = loan_amount
      product
    end

    def product
      @product_finance = ProductFinance.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id)
      @product_rental = ProductRentalCost.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id, regional_id: @regional_id)
      @product_admin_costs = ProductAdminCost.find_by(product_id: @product_id, insurance_item: @insurance_item_id, @loan_amount => :min_loan_range..:max_loan_range)
      @product_finance_item = ProductFinanceItem.find_by(product_finance_id: @product_finance.id, regional_id: @regional_id)
    end

    def ltv
      ltv_deviation = ProductDeviationLtv.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id, regional_id: @regional_id)
      ltv_deviation[:base] = @product_finance_item.ltv_value
      ltv_deviation
    end

    def admin
      admin_deviation = ProductDeviationAdmin.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id)
      admin_deviation[:base] = @product_admin_costs.cost_nominal
      admin_deviation
    end

    def rental
      rental_deviation = ProductDeviationRental.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id, regional_id: @regional_id)
      rental_deviation[:base] = @product_rental[:rental_cost_percentage]
      rental_deviation
    end

    def insurance_item
      insurance_item_deviation = ProductDeviationInsuranceItem.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id)
      insurance_item_deviation[:base_karats] = @product_finance[:min_karatase]
      insurance_item_deviation[:base_weight] = @product_finance[:min_weight]
      insurance_item_deviation
    end

    def one_obligor
      one_obligor_deviation = ProductDeviationOneObligor.find_by(product_id: @product_id, insurance_item_id: @insurance_item_id)
      one_obligor_deviation[:base] = @product_finance.product.one_obligor_value
      one_obligor_deviation
    end

    def execute
      @deviation = {
        one_obligor: one_obligor,
        rental: rental,
        admin: admin,
        ltv: ltv
        # insurance_item: insurance_item
      }
    end
  end
end
