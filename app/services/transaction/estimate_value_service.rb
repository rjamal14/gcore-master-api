module Transaction
  # Estimate Value Service
  class EstimateValueService
    def initialize(carats:, weight:, product_id:, insurance_item_id:, region_id:)
      @carats = carats.to_f
      @weight = weight.to_f
      product_finance = ProductFinance.find_by(product_id: product_id,
                                               insurance_item_id: insurance_item_id)
      @product_finance_item = product_finance.product_finance_items.find_by(regional_id: region_id)
    end

    def est_val_hash
      {
        estimated_value: est_value,
        ltv: @product_finance_item[:ltv_value].to_f,
        ltv_value: (@product_finance_item[:ltv_value].to_f * est_value.to_f / 100).round(2)
      }
    end

    def est_value
      (@product_finance_item[:stle_value].to_f * @weight * @carats / 24).round(2)
    end

    def execute
      est_val_hash
    end
  end
end
