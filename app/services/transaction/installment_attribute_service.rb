module Transaction
  class InstallmentAttributeService
    def initialize(data:, office_id:, office_type:)
      @estimated_value = 0
      @financing_amount = 0
      @buy_price = 0
      @product = Product.find(data[:product_id])
      @region_id = find_region_id(office_id, office_type)
      finance = find_slte_and_ltv(@region_id, data[:insurance_item_id])
      @data = {
        ltv: finance[:ltv], stle: finance[:stle], tenor: data[:tenor], insurance_item_id: data[:insurance_item_id]
      }
      @rental_cost = find_rental_cost(@region_id, data[:insurance_item_id])
      data[:insurance_items].map { |insurance_item| count_insurance_item(insurance_item) }
    end

    def find_slte_and_ltv(region_id, insurance_item_id)
      product_finance = @product.product_finances.find_by(insurance_item_id: insurance_item_id)
      product_finance_item = product_finance.product_finance_items.find_by(regional_id: region_id)
      {
        ltv: product_finance_item.ltv_value,
        stle: product_finance_item.stle_value
      }
    end

    def find_region_id(office_id, office_type)
      if office_type == 'unit'
        UnitOffice.find(office_id).try(:branch_office).try(:area).try(:regional_id)
      else
        BranchOffice.find(office_id).try(:area).try(:regional_id)
      end
    end

    def find_rental_cost(region_id, insurance_item_id)
      @product.product_rental_costs.find_by(regional_id: region_id,
                                            insurance_item_id: insurance_item_id).try(:rental_cost_percentage)
    end

    def count_insurance_item(data)
      @estimated_value += data[:weight] * @data[:stle] * data[:karats] / 24 * data[:amount]
      @financing_amount += data[:weight] * @data[:stle] * data[:karats] / 24 * @data[:ltv] / 100 * data[:amount]
      @buy_price += data[:buy_price] * data[:amount]
    end

    def installment_attribute_hash
      {
        buy_price: @buy_price,
        estimated_value: @estimated_value.to_i,
        maximum_financing_amount: @financing_amount.to_i,
        monthly_fee: (@financing_amount * @rental_cost / 100).to_i,
        monthly_interest: @rental_cost,
        ltv: @data[:ltv],
        minimum_down_payment: (@buy_price - @financing_amount).to_i
      }
    end

    def execute
      installment_attribute_hash
    end
  end
end
