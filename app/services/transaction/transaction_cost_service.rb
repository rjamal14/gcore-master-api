module Transaction
  class TransactionCostService
    def initialize(params)
      @params = params
      @tiering_type = Product.find(@params[:product_id]).try(:tiering_type)
      @rental_fee = ProductRentalCost.find_by(product_id: @params[:product_id],
                                              insurance_item_id: @params[:insurance_item_id],
                                              regional_id: check_region_id)
    end

    def transaction_cost_hash
      {
        admin_fee: if @tiering_type == 'nominal'
                     admin_fee.try(:cost_nominal)
                   else
                     admin_fee.try(:cost_percentage) * @params[:loan_nominal].to_i / 100
                   end,
        rental_cost_per_15_days: @rental_fee[:rental_cost_percentage_15_days] / 100 * @params[:loan_nominal].to_i,
        rental_cost_per_month: @rental_fee[:rental_cost_percentage] / 100 * @params[:loan_nominal].to_i,
        rental_percentage_per_15_days: @rental_fee[:rental_cost_percentage_15_days],
        rental_percentage_per_month: @rental_fee[:rental_cost_percentage]
      }
    end

    def admin_fee
      admin_fees = ProductAdminCost.where('max_loan_range' => { '$gte' => @params[:loan_nominal] },
                                          'min_loan_range' => { '$lte' => @params[:loan_nominal] })
      admin_fees.find_by(product_id: @params[:product_id], insurance_item_id: @params[:insurance_item_id])
    end

    def check_region_id
      case @params[:office_type]
      when 'branch'
        BranchOffice.find(@params[:office_id]).try(:area).try(:regional_id) || 'default'
      when 'unit'
        UnitOffice.find(@params[:office_id]).try(:branch_office).try(:area).try(:regional_id) || 'default'
      when 'area'
        Area.find(@params[:office_id]).try(:regional_id) || 'default'
      else
        @params[:office_id]
      end
    end

    def merge_option
      @params[:option] == 'weekly' ? transaction_cost_hash.merge(weekly_hash) : transaction_cost_hash.merge(daily_hash)
    end

    def daily_hash
      {
        rental_percentage_per_day: @rental_fee[:rental_cost_percentage].to_f / 30,
        rental_cost_per_day: @rental_fee[:rental_cost_percentage].to_f / 100 * @params[:loan_nominal].to_i / 30
      }
    end

    def weekly_hash
      {
        rental_percentage_per_week: @rental_fee[:rental_cost_percentage].to_f / 4,
        rental_cost_per_week: @rental_fee[:rental_cost_percentage].to_f / 100 * @params[:loan_nominal].to_i / 4
      }
    end

    def execute
      @params.key?(:option) ? merge_option : transaction_cost_hash
    end
  end
end
