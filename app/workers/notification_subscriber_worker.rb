require 'json'
require 'base64'

class NotificationSubscriberWorker
  include Sneakers::Worker

  queue_name = "#{ENV['rbmq_env']}-notifications-queue"
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    msg = ActiveSupport::JSON.decode(msg)
    notification = Notifications::PublishNotificationService.new(msg: msg).execute
    if notification == 'failed'
      logger.fatal "Failed when saving notification => #{notification.errors}"
      reject!
    else
      logger.info 'Notification saved'
      ack!
    end
  rescue StandardError => e
    logger.fatal "Error message JournalWorker => #{e.message}"
    reject!
  end
end
