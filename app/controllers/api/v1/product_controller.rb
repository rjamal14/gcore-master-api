# ProductController
module Api
  module V1
    # ProductController
    class ProductController < ApplicationController
      before_action :set_product, only: [:show, :update, :insurance_item_getter, :prolongation_getter, :fine_product_getter]
      skip_before_action :current_owner, only: [:autocomplete, :insurance_item_getter, :prolongation_getter, :fine_product_getter]

      # GET /product
      def index
        products = params.key?(:type) ? check_product_index : Product.not.where(type: :installment)
        products = params.key?(:search) ? products.where(name: /.*#{params[:search]}.*/i) : products
        products = products.page(params[:page] || 1).per(params[:per_page] || 10)
                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_products = products.map { |product| ProductSerializer.new(product, root: false) }

        response_index(products, serial_products)
      end

      # GET /product/1
      def show
        @product = ProductDetailSerializer.new(@product)
        response_success(@product, 'Data Ditemukan')
      end

      def autocomplete
        products = Product.where(name: /.*#{params[:query]}.*/i, type: params[:type] || 'regular')
                          .order_by(name: :asc)
                          .limit(10)
        products = products.map { |product| ProductAutocompleteSerializer.new(product, root: false) }
        if products.present?
          response_success(products, 'Berhasil')
        else
          response_success(products, 'data kosong / tidak ditemukan')
        end
      end

      # POST /product
      def create
        product = Product.new(product_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if product.save
          product = ProductDetailSerializer.new(product)
          response_created(product, 'Data Product Tersimpan')
        else
          response_error(product, 'Data Product Gagal Tersimpan')
        end
      rescue StandardError => e
        response_bad(e)
      end

      # PATCH/PUT /product/1
      def update
        if @product.update(product_params.merge(updated_by_id: current_user))
          @product = ProductDetailSerializer.new(@product)
          response_updated(@product, 'Data Product Terupdate')
        else
          response_error(@product, 'Data Product Gagal Terupdate')
        end
      rescue StandardError => e
        response_bad(e)
      end

      # DELETE /product/1
      def destroy
        if params.key?(:id)
          products = Product.in(id: params[:id].split(','))
          if products.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            products.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      def insurance_item_getter
        insurance_items = InsuranceItemProductSerializer.new(@product)
        response_success(insurance_items, 'Data ditemukan')
      end

      def prolongation_getter
        admin = @product.product_prolongations.admin.find_by(insurance_item_id: params[:insurance_item_id],
                                                             order: params[:order]).value
        rental = @product.product_prolongations.rental.find_by(insurance_item_id: params[:insurance_item_id],
                                                               order: params[:order]).value

        render json: {
          admin: admin,
          rental: rental
        }
      rescue StandardError => e
        response_bad(e)
      end

      def fine_product_getter
        render json: {
          grace_period: @product.product_period.grace_period,
          fine_percentage: @product.product_period.fine_percentage
        }
      rescue StandardError => e
        response_bad(e)
      end

      private

      def check_product_index
        case params[:type]
        when 'all'
          Product
        when 'installment'
          Product.where(type: :installment)
        when 'option'
          Product.where(type: :option)
        else
          Product.where(type: :installment)
        end
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_product
        @product = Product.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_params
        params.require(:product).permit(:name, :product_image, :status, :tiering_type, :type, :one_obligor_value,
                                        product_admin_costs_attributes: [:id, :min_loan_range, :max_loan_range,
                                                                         :cost_nominal, :cost_percentage,
                                                                         :insurance_item_id, :_destroy],
                                        product_admin_cost_discounts_attributes: [:id, :repawn_1, :repawn_2, :repawn_3,
                                                                                  :repawn_4, :repawn_5, :repawn_6,
                                                                                  :repawn_7, :repawn_8, :repawn_9,
                                                                                  :repawn_10, :repawn_11, :repawn_12,
                                                                                  :insurance_item_id, :_destroy],
                                        product_deviation_ltv_attributes: [:id, :insurance_item_id, :_destroy,
                                                                           :area, :region, :branch, :head, :regional_id],
                                        product_deviation_rentals_attributes: [:id, :insurance_item_id, :_destroy,
                                                                               :area, :region, :branch, :head, :regional_id],
                                        product_deviation_admins_attributes: [:id, :insurance_item_id, :_destroy,
                                                                              :branch, :area, :region, :head],
                                        product_deviation_one_obligors_attributes: [:id, :insurance_item_id, :_destroy,
                                                                                    :branch, :area, :region, :head],
                                        product_deviation_insurance_items_attributes: [:id, :insurance_item_id, :_destroy,
                                                                                       :area_karats, :region_karats, :branch_karats, :head_karats,
                                                                                       :area_weight, :region_weight, :branch_weight, :head_weight],
                                        product_finances_attributes: [:id, :insurance_item_id, :min_karatase, :min_loan, :max_loan, :min_weight, :_destroy,
                                                                      { product_finance_items_attributes: [:id, :ltv_value, :stle_value, :regional_id, :_destroy] }],
                                        product_insurance_items_attributes: [:id, :insurance_item_id, :name,
                                                                             :minimum_karatase, :certificate, :_destroy],
                                        product_period_attributes: [:id, :province_id, :period_of_time, :due_date, :grace_period,
                                                                    :fine_amount, :fine_percentage, :auction_period, :_destroy],
                                        product_rental_costs_attributes: [:id, :rental_cost_percentage, :insurance_item_id,
                                                                          :rental_cost_percentage_15_days, :regional_id, :_destroy],
                                        product_prolongations_attributes: [:id, :value, :insurance_item_id, :order, :type, :_destroy])
      end
    end
  end
end
