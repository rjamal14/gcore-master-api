# ProductDeviationItemController
module Api
  module V1
    # ProductDeviationItemController
    class ProductDeviationItemController < ApplicationController
      before_action :set_product_deviation_item, only: [:show, :update, :destroy]
      def index
        @product_deviation_item = if params[:search]
                                    ProductDeviationItem.where(product_id: params[:search])
                                  else
                                    ProductDeviationItem
                                  end

        @product_deviation_item = @product_deviation_item.page(params[:page] || 1).per(params[:per_page] || 10)
                                                         .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_product_deviation_item = @product_deviation_item.map { |product_deviation_item| ProductDeviationItemSerializer.new(product_deviation_item, root: false) }

        response_index(@product_deviation_item, serial_product_deviation_item)
      end

      # GET /product_deviation_item/1
      def show
        response_success(@product_deviation_item, 'Data Ditemukan')
      end

      # POST /product_deviation_item
      def create
        p product_deviation_item_params
        @product_deviation_item = ProductDeviationItem.new(product_deviation_item_params.merge(product_id: params[:product_id]))

        if @product_deviation_item.save
          response_created(@product_deviation_item, 'Data ProductDeviationItem Tersimpan')
        else
          response_error(@product_deviation_item, 'Data ProductDeviationItem Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_deviation_item/1
      def update
        if @product_deviation_item.update(product_deviation_item_params)
          response_updated(@product_deviation_item, 'Data ProductDeviationItem Terupdate')
        else
          response_error(@product_deviation_item, 'Data ProductDeviationItem Gagal Terupdate')
        end
      end

      # DELETE /product_deviation_item/1
      def destroy
        @product_deviation_item.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_deviation_item
        @product_deviation_item = ProductDeviationItem.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_deviation_item_params
        dev_params = params[:product_deviation_item]
        load_params = params.require(:product_deviation_item).permit(:name, :insurance_item_id)
        load_params[:branch] = dev_params[:branch] if dev_params[:branch]
        load_params[:area] = dev_params[:area] if dev_params[:area]
        load_params[:region] = dev_params[:region] if dev_params[:region]
        load_params[:headquarters] = dev_params[:headquarters] if dev_params[:headquarters]

        load_params.permit!
      end
    end
  end
end
