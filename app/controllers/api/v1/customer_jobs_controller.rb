# Class Customer Job
module Api
  module V1
    # Class Customer Job
    class CustomerJobsController < ApplicationController
      before_action :set_customer_job, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /customer_jobs
      def index
        @customer_jobs = if params[:search]
                           CustomerJob.where(name: /.*#{params[:search]}.*/i)
                         else
                           CustomerJob
                         end
        @customer_jobs = @customer_jobs.page(params[:page] || 1).per(params[:per_page] || 10)
                                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_customer_jobs = @customer_jobs.map { |customer_jobs| CustomerJobSerializer.new(customer_jobs, root: false) }

        response_index(@customer_jobs, serial_customer_jobs)
      end

      def autocomplete
        @customer_jobs = CustomerJob.only(:id, :name)
                                    .where(name: /.*#{params[:query]}.*/i)
                                    .order_by(name: :asc)
                                    .limit(10)
        response_success(@customer_jobs, 'Data yang ditemukan')
      end

      # GET /customer_jobs/1
      def show
        response_success(@customer_job, 'Data Pekerjaan Ditemukan')
      end

      # POST /customer_jobs
      def create
        @customer_job = CustomerJob.new(customer_job_params.merge(created_by_id: current_user))

        if @customer_job.save
          response_created(@customer_job, 'Data Pekerjaan tersimpan')
        else
          response_error(@customer_job, 'Data Pekerjaan gagal tersimpan')
        end
      end

      # PATCH/PUT /customer_jobs/1
      def update
        if @customer_job.update(customer_job_params.merge(updated_by_id: current_user))
          response_updated(@customer_job, 'Data Pekerjaan terupdate')
        else
          response_error(@customer_job, 'Data Pekerjaan gagal teupdate')
        end
      end

      # DELETE /customer_jobs/1
      def destroy
        if params.key?(:id)
          @customer_jobs = CustomerJob.in(id: params[:id].split(','))
          if @customer_jobs.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @customer_jobs.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_customer_job
        @customer_job = CustomerJob.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def customer_job_params
        params.require(:customer_job).permit(:name, :description)
      end
    end
  end
end
