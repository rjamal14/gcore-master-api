# City Controller
module Api
  module V1
    # City Controller
    class CitiesController < ApplicationController
      before_action :set_city, only: [:show, :update]
      before_action :set_params, only: [:index, :index_by_province, :autocomplete]
      before_action :set_cities_per_province, only: :autocomplete
      skip_before_action :current_owner, only: [:autocomplete, :array_city]
      # GET /cities
      def index
        cities = params[:search] ? City.where(name: /.*#{params[:search]}.*/i) : City
        cities = cities.page(params[:page] || 1).per(params[:per_page] || 10)
                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_cities = cities.map { |city| CitySerializer.new(city, root: false) }

        response_index(cities, serial_cities)
      end

      def array_city
        cities = City.find params[:data]
        cities = cities.map { |city| CitySerializer.new(city, root: false) }
        render json: { cities: cities }
      end

      def index_by_province
        if params.key?(:province_id)
          cities = City.where(province_id: params[:province_id]).only(:id, :name)
          if cities.blank?
            response_bad('Data kosong')
          else
            response_success(cities, 'Data kota ditemukan')
          end
        else
          response_bad('Parameter tidak lengkap')
        end
      end

      def autocomplete
        @cities = @cities.only(:id, :name, :province_id, :city_code).order_by(name: :asc).limit(10)
        @cities = @cities.map { |city| CitySerializer.new(city, root: false) }
        if @cities.present?
          response_success(@cities, 'Berhasil')
        else
          response_success(@cities, 'data kosong / tidak ditemukan')
        end
      end

      # GET /cities/1
      def show
        city_serializer = CitySerializer.new(@city)
        response_success(city_serializer, 'Data Ditemukan')
      end

      # POST /cities
      def create
        city = City.new(city_params.merge(created_by_id: current_user))
        city.province_id = province_id_setter(city.province_id)
        city.save ? response_created(city, 'Data Kota Tersimpan') : response_error(city, 'Data Kota Gagal Tersimpan')
      end

      # PATCH/PUT /cities/1
      def update
        @city.update(city_params.merge(updated_by_id: current_user))
        @city.province_id = province_id_setter(@city.province_id)
        if @city.save
          response_updated(@city, 'Data Kota Terupdate')
        else
          response_error(@city, 'Data Kota Gagal Terupdate')
        end
      end

      # DELETE /cities/1
      def destroy
        if params.key?(:id)
          @cities = City.in(id: params[:id].split(','))
          if @cities.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @cities.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      def set_params
        params[:province_id] = params[:province_id].to_i if params.key?(:province_id)
      end

      def province_id_setter(attr_province)
        province = Province.where(province_code: attr_province).last
        if province.present?
          province.province_code
        else
          Province.where(id: attr_province).last.province_code
        end
      end

      def set_cities_per_province
        @cities = if params.key?(:province_id)
                    City.where(name: /.*#{params[:query]}.*/i, province_id: params[:province_id])
                  else
                    City.where(name: /.*#{params[:query]}.*/i)
                  end
      end

      def set_city
        @city = begin
          City.find(params[:id])
        rescue StandardError
          response_not_found('Data Tidak Ditemukan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def city_params
        params.require(:city).permit(:name, :province_id, :city_code)
      end
    end
  end
end
