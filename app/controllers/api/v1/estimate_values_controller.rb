module Api
  module V1
    # Bank Controller
    class EstimateValuesController < ApplicationController
      before_action :check_params
      def check_estimated_value
        region_id = Area.find_by(code: current_owner.area_code).regional_id
        estimate = Transaction::EstimateValueService.new(carats: params[:carats],
                                                         weight: params[:weight],
                                                         product_id: params[:product_insurance_item_id],
                                                         insurance_item_id: params[:insurance_item_id],
                                                         region_id: region_id)
        response_success(estimate.execute, 'Hasil Perhitungan Estimated Value')
      rescue StandardError => e
        response_bad("Biaya Admin Belum tersedia, error: #{e}")
      end
      # Only allow a trusted parameter "white list" through.

      private

      def check_params
        response_bad('Parameter Tidak Lengkap') unless params.key?(:carats) && params.key?(:weight) &&
                                                       params.key?(:product_insurance_item_id) &&
                                                       params[:insurance_item_id]
      end
    end
  end
end
