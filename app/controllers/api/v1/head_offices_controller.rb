# Controller Kantor Cabang
module Api
  module V1
    # Controller Kantor Cabang
    class HeadOfficesController < ApplicationController
      before_action :set_head_office, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /head_offices
      def index
        head_offices = params[:search] ? HeadOffice.where(name: /.*#{params[:search]}.*/i) : HeadOffice
        head_offices = head_offices.page(params[:page] || 1).per(params[:per_page] || 10)
                                   .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_head_offices = head_offices.map { |head_office| HeadOfficeSerializer.new(head_office, root: false) }

        response_index(head_offices, serial_head_offices)
      end

      def autocomplete
        head_offices = HeadOffice.only(:id, :name, :city_id)
                                 .where(name: /.*#{params[:query]}.*/i)
                                 .order_by(name: :asc)
                                 .limit(10)
        response_success(head_offices, 'Data yang ditemukan')
      end

      # GET /head_offices/1
      def show
        if @head_office.present?
          @head_office = HeadOfficeDetailSerializer.new(@head_office, root: false)
          response_success(@head_office, 'Data Ditemukan')
        else
          response_bad('Data Tidak Ditemukan')
        end
      end

      # POST /head_offices
      def create
        head_office = HeadOffice.new(head_office_params.merge(created_by_id: current_user))

        if head_office.save
          response_created(head_office, 'Data kantor pusat tersimpan')
        else
          response_error(head_office, 'Data kantor pusat gagal tersimpan')
        end
      end

      # PATCH/PUT /head_offices/1
      def update
        if @head_office.update(head_office_params.merge(updated_by_id: current_user))
          response_updated(@head_office, 'Data kantor pusat terupdate')
        else
          response_error(@head_office, 'Data kantor pusat gagal terupdate')
        end
      end

      # DELETE /head_offices/1
      def destroy
        if params.key?(:id)
          head_offices = HeadOffice.in(id: params[:id].split(','))
          if head_offices.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            head_offices.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_head_office
        @head_office = HeadOffice.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def head_office_params
        params.require(:head_office).permit(:name, :code, :city_id, :address, :latitude, :longitude, :description)
      end
    end
  end
end
