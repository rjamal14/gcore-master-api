module Api
  module V1
    # GoldMarketPrice Controller
    class GoldMarketPriceController < ApplicationController
      before_action :set_gold_market_price, only: [:show, :update]

      # GET /gold_market_price
      def index
        gold_market_prices = params[:search] ? GoldMarketPrice.where(valid_date: params[:search]) : GoldMarketPrice
        gold_market_prices = gold_market_prices.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_gold_market_prices = gold_market_prices.map { |gold_market_price| GoldMarketPriceSerializer.new(gold_market_price, root: false) }
        response_index(gold_market_prices, serial_gold_market_prices)
      end

      def list
        @gold_market_price = GoldMarketPrice.where(valid_date: Date.today).first

        render json: { message: 'Not Found' }, status: 404 unless @gold_market_price.present?

        gold_market_price_data = []

        (1..24).each do |i|
          gold_market_price_data.push({
                                        karatase: i,
                                        loan_rate: (i.to_f / 24).to_f * @gold_market_price.price
                                      })
        end

        p gold_market_price_data

        render json: { code: 200, message: 'success', data: gold_market_price_data.sort_by(&:first).reverse }, status: 200
      end

      # GET /gold_market_price/1
      def show
        response_success(@gold_market_price, 'Data Ditemukan')
      end

      # POST /gold_market_price
      def create
        @gold_market_price = GoldMarketPrice.new(gold_market_price_params)
        if @gold_market_price.save
          response_created(@gold_market_price, 'Data GoldMarketPrice Tersimpan')
        else
          p @gold_market_price
          response_error(@gold_market_price, 'Data GoldMarketPrice Gagal Tersimpan')
        end
      end

      # PATCH/PUT /gold_market_price/1
      def update
        if @gold_market_price.update
          response_updated(@gold_market_price, 'Data GoldMarketPrice Terupdate')
        else
          response_error(@gold_market_price, 'Data GoldMarketPrice Gagal Terupdate')
        end
      end

      # DELETE /gold_market_price/1
      def destroy
        if params.key?(:id)
          @gold_market_prices = GoldMarketPrice.in(id: params[:id].split(','))
          if @gold_market_prices.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @gold_market_prices.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_gold_market_price
        @gold_market_price = GoldMarketPrice.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def gold_market_price_params
        params.require(:gold_market_price).permit(:price, :valid_date, :insurance_item_id)
      end
    end
  end
end
