module Api
  # Module V1
  module V1
    # Class Countrues
    class CountriesController < ApplicationController
      before_action :set_country, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete
      # GET /countries
      def index
        countries = Country.page(params[:page] || 1).per(params[:per_page] || 10)
                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        countries = params[:search] ? countries.where(name: /.*#{params[:search]}.*/i) : countries

        serial_countries = countries.map { |country| CountrySerializer.new(country, root: false) }
        response_index(countries, serial_countries)
      end

      def autocomplete
        countries = Country.where(name: /.*#{params[:query]}.*/i).order_by(name: :asc).limit(10)
        countries = countries.map { |country| CountrySerializer.new(country, root: false) }
        if countries.present?
          response_success(countries, 'Berhasil')
        else
          response_success(countries, 'data kosong / tidak ditemukan')
        end
      end

      # GET /countries/1
      def show
        render json: @country, serializer: CountrySerializer
      end

      # POST /countries
      def create
        country = Country.new(country_params.merge(created_by_id: current_user))

        if country.save
          response_created(country, 'Data Negara Tersimpan')
        else
          response_error(country, 'Data Negara Gagal Tersimpan')
        end
      end

      # PATCH/PUT /countries/1
      def update
        if @country
          response_updated(@country, 'Behasil Update Data Negara') if @country.update(country_params.merge(updated_by_id: current_user))
        else
          response_error(@country, 'Update Data Negara Gagal')
        end
      end

      # DELETE /countries/1
      def destroy
        if params.key?(:id)
          countries = Country.in(id: params[:id].split(','))
          if countries.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            countries.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data Negara')
        end
      rescue StandardError => e
        response_bad(e)
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_country
        @country = Country.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def country_params
        params.require(:country).permit(:name, :nationality)
      end
    end
  end
end
