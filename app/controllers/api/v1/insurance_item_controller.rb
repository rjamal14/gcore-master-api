# Ltv InsuranceItemController
module Api
  module V1
    # Ltv InsuranceItemController
    class InsuranceItemController < ApplicationController
      before_action :set_insurance_item, only: [:show, :update]
      skip_before_action :current_owner, only: [:autocomplete, :show]

      # GET /insurance_item
      def index
        @insurance_items = params[:search] ? InsuranceItem.where(name: /.*#{params[:search]}.*/i) : InsuranceItem
        @insurance_items = @insurance_items.page(params[:page] || 1).per(params[:per_page] || 10)
                                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        response_index(@insurance_items, serial_insurance_items)
      end

      def autocomplete
        insurance_items = InsuranceItem.only(:id, :name)
                                       .where(name: /.*#{params[:query]}.*/i)
                                       .order_by(name: :asc)
                                       .limit(10)
        if insurance_items.present?
          response_success(insurance_items, 'Berhasil')
        else
          response_bad('data kosong / tidak ditemukan')
        end
      end

      # GET /insurance_item/1
      def show
        insurance_item = @insurance_item.attributes
        insurance_item['insurance_item_types'] = @insurance_item.insurance_item_types
        response_success(insurance_item, 'Data Ditemukan')
      end

      # POST /insurance_item
      def create
        insurance_item = InsuranceItem.new(insurance_item_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if insurance_item.save
          response_created(insurance_item, 'Data Barang Jaminan Tersimpan')
        else
          response_error(insurance_item, 'Data Barang Jaminan Gagal Tersimpan')
        end
      end

      # PATCH/PUT /insurance_item/1
      def update
        if @insurance_item.update(insurance_item_params.merge(updated_by_id: current_user))
          response_updated(@insurance_item, 'Data Barang Jaminan Terupdate')
        else
          response_error(@insurance_item, 'Data Barang Jaminan Gagal Terupdate')
        end
      end

      # DELETE /insurance_item/1
      def destroy
        if params.key?(:id)
          insurance_items = InsuranceItem.in(id: params[:id].split(','))
          if insurance_items.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            insurance_items.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      def graph
        insurance_items = InsuranceItem.only(:id, :name)
        insurance_items = insurance_items.map { |obj| obj.attributes.merge({ total_insurance_item: total("insurance_item_id=#{obj.id}") }) }
        render json: insurance_items
      end

      private

      def total(params)
        url = 'total_insurance_items'
        get_transaction(url, params)
      end

      def serial_insurance_items
        @insurance_items.map do |insurance_item|
          insurance_item_params = params.key?(:date) ? "insurance_item_id=#{insurance_item.id}&date=#{params[:date]}" : "insurance_item_id=#{insurance_item.id}"
          insurance_item.attributes.merge({ total_insurance_item: total(insurance_item_params) })
        end
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_insurance_item
        @insurance_item = InsuranceItem.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def insurance_item_params
        params.require(:insurance_item).permit(:name, :status, :description, insurance_item_types_attributes: [:id, :name, :_destroy])
      end
    end
  end
end
