# City Controller
module Api
  module V1
    # City Controller
    class AddressController < ApplicationController
      skip_before_action :current_owner, only: [:get_data]

      def data_getter
        data = {
          subdistric_name: Urban.find(params[:subdistric]).try(:name),
          city_name: City.find(params[:city]).try(:name),
          region_name: District.find(params[:region]).try(:name),
          province_name: Province.find(params[:province]).try(:name)
        }

        response_success(data, 'Data Ditemukan')
      end

      def find
        data = {
          subdistric: Urban.where(name: /.*#{params[:subdistric]}.*/i).last.try(:id).to_s,
          city: City.where(name: /.*#{params[:city]}.*/i).last.try(:id).to_s,
          region: District.where(name: /.*#{params[:region]}.*/i).last.try(:id).to_s,
          province: Province.where(name: /.*#{params[:province]}.*/i).last.try(:id).to_s
        }

        response_success(data, 'Data Ditemukan')
      end
    end
  end
end
