# ProductRentalCostController
module Api
  module V1
    # Product Rental Cost
    class ProductRentalCostController < ApplicationController
      before_action :set_product_rental_cost, only: [:show, :update, :destroy]
      def index
        @product_rental_cost = if params[:search]
                                 ProductRentalCost.where(product_id: params[:search])
                               else
                                 ProductRentalCost
                               end

        @product_rental_cost = @product_rental_cost.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_rental_cost = @product_rental_cost.map { |product_rental_cost| ProductRentalCostSerializer.new(product_rental_cost, root: false) }
        response_index(@product_rental_cost, serial_product_rental_cost)
      end

      # GET /product_rental_cost/1
      def show
        response_success(@product_rental_cost, 'Data Ditemukan')
      end

      # POST /product_rental_cost
      def create
        @product_rental_cost = ProductRentalCost.new(product_rental_cost_params.merge(product_id: params[:product_id]))

        if @product_rental_cost.save
          response_created(@product_rental_cost, 'Data ProductRentalCost Tersimpan')
        else
          response_error(@product_rental_cost, 'Data ProductRentalCost Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_rental_cost/1
      def update
        if @product_rental_cost.update(product_rental_cost_params)
          response_updated(@product_rental_cost, 'Data ProductRentalCost Terupdate')
        else
          response_error(@product_rental_cost, 'Data ProductRentalCost Gagal Terupdate')
        end
      end

      # DELETE /product_rental_cost/1
      def destroy
        @product_rental_cost.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_rental_cost
        @product_rental_cost = ProductRentalCost.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_rental_cost_params
        params.require(:product_rental_cost).permit(:rental_cost_percentage, :day_period, :insurance_item_id)
      end
    end
  end
end
