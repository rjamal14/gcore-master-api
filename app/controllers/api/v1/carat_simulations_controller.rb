# CaratSimulationsController
module Api
  module V1
    # CaratSimulationsController
    class CaratSimulationsController < ApplicationController
      def list
        @slte = Slte.where(valid_date: Date.today, status: true).first

        render json: { message: 'Not Found' }, status: 404 unless @slte.present?

        carat_simulation_data = []

        (1..24).each do |i|
          carat_simulation_data.push({
                                       karatase: i,
                                       loan_rate: (i / 24).to_f * @slte.value_slte
                                     })
        end

        render json: { code: 200, message: 'success', data: carat_simulation_data.sort_by(&:first).reverse }, status: 200
      end
    end
  end
end
