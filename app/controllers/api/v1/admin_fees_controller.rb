module Api
  module V1
    # Admin Fees COntroller
    class AdminFeesController < ApplicationController
      before_action :check_params
      def fetch_nominal
        @nominal = ProductAdminCost.where(product_id: params[:product_id], insurance_item_id: params[:insurance_item_id],
                                          'max_loan_range' => { '$gte' => params[:loan_nominal] },
                                          'min_loan_range' => { '$lte' => params[:loan_nominal] })
        check_tiering_type
        if !@nominal.empty?
          response_success(@nominal, 'Data Ditemukan')
        else
          response_bad('Data Kosong')
        end
      end

      private

      def check_params
        response_bad('Parameter tidak lengkap') unless params.key?(:product_id) && params.key?(:loan_nominal) && params.key?(:product_id)
      end

      def check_tiering_type
        @product_tiering_type = Product.find(params[:product_id])[:tiering_type]
        @nominal = if @product_tiering_type == 'nominal'
                     @nominal.only(:cost_nominal)
                   else
                     @nominal.only(:cost_percentage)
                   end
      rescue StandardError
        nil
      end
    end
  end
end
