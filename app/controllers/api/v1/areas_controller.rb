# area Controller
module Api
  module V1
    # area Controller
    class AreasController < ApplicationController
      before_action :set_area, only: [:show, :update]
      skip_before_action :current_owner, only: [:autocomplete, :logo_company]

      # GET /areas
      def index
        areas = params[:search] ? Area.where(name: /.*#{params[:search]}.*/i) : Area
        areas = areas.page(params[:page] || 1).per(params[:per_page] || 10)
                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_areas = areas.map { |area| AreaSerializer.new(area, root: false) }

        response_index(areas, serial_areas)
      end

      def autocomplete
        areas = Area.only(:id, :name)
                    .where(name: /.*#{params[:query]}.*/i)
                    .order_by(name: :asc)
                    .limit(10)
        if areas.present?
          response_success(areas, 'Berhasil')
        else
          response_success(areas, 'data kosong / tidak ditemukan')
        end
      end

      def logo_company
        area = UnitOffice.find(params[:office_id])
                         .try(:branch_office)
                         .try(:area)
                         .try(:regional)
        logo_company = {
          logo: {
            url: area.try(:logo).try(:url)
          },
          company_name: area.try(:company_name)
        }

        if area.present?
          response_success(logo_company, 'Berhasil')
        else
          response_success(logo_company, 'data kosong / tidak ditemukan')
        end
      end

      # GET /areas/1
      def show
        @area = AreaDetailSerializer.new(@area, root: false)
        response_success(@area, 'Data Ditemukan')
      end

      # POST /areas
      def create
        area = Area.new(area_params.merge(created_by_id: current_user))

        if area.save
          response_created(area, 'Data Area Tersimpan')
        else
          response_error(area, 'Data Area Gagal Tersimpan')
        end
      end

      # PATCH/PUT /areas/1
      def update
        if @area.update(area_params.merge(updated_by_id: current_user))
          response_updated(@area, 'Data Area Terupdate')
        else
          response_error(@area, 'Data Area Gagal Terupdate')
        end
      end

      # DELETE /areas/1
      def destroy
        if params.key?(:id)
          areas = Area.in(id: params[:id].split(','))
          if areas.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            areas.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      rescue StandardError => e
        response_bad(e)
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_area
        @area = Area.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def area_params
        params.require(:area).permit(:name, :code, :city_id, :regional_id, :address, :latitude, :longitude, :description, :company_name)
      end
    end
  end
end
