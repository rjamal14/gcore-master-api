# OfficeHourSchedulesController
module Api
  module V1
    # OfficeHourSchedulesController
    class OfficeHourSchedulesController < ApplicationController
      before_action :set_office_schedule, only: [:show, :update]

      # GET /office_schedule
      def index
        @office_schedule = if params[:search]
                             OfficeHourSchedule.where(schedule_date: params[:search])
                           else
                             OfficeHourSchedule
                           end
        @office_schedule = @office_schedule.page(params[:page] || 1).per(params[:per_page] || 10)
                                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_office_schedule = @office_schedule.map { |office_schedule| OfficeHourSchedulesSerializer.new(office_schedule, root: false) }
        response_index(@office_schedule, serial_office_schedule)
      end

      # GET /office_schedule/1
      def show
        response_success(@office_schedule, 'Data Ditemukan')
      end

      # POST /office_schedule
      def create
        @office_schedule = OfficeHourSchedule.new(office_schedule_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if @office_schedule.save
          response_created(@office_schedule, 'Data Jam Kerja Tersimpan')
        else
          response_error(@office_schedule, 'Data Jam Kerja Gagal Tersimpan')
        end
      end

      # PATCH/PUT /office_schedule/1
      def update
        if @office_schedule.update(office_schedule_params.merge(updated_by_id: current_user))
          response_updated(@office_schedule, 'Data Jam Kerja Terupdate')
        else
          response_error(@office_schedule, 'Data Jam Kerja Gagal Terupdate')
        end
      end

      # DELETE /office_schedule/1
      def destroy
        if params.key?(:id)
          @office_schedules = OfficeHourSchedule.in(id: params[:id].split(','))
          if @office_schedules.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @office_schedules.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_office_schedule
        @office_schedule = OfficeHourSchedule.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def office_schedule_params
        params.require(:office_hour_schedules).permit(:area_id, :schedule_date, :started_at, :closed_at)
      end
    end
  end
end
