module Api
  module V1
    # Rental Cost Controller
    class RentalCostsController < ApplicationController
      before_action :check_params, :check_region
      def check_rental_cost_value
        rental_cost = ProductRentalCost.find_by(product_id: params[:product_id],
                                                insurance_item_id: params[:insurance_item_id], regional_id: @region_id)
        rental_cost[:rental_cost] = rental_cost[:rental_cost_percentage] / 100 * params[:loan_value].to_i
        rental_cost[:rental_cost_15_days] = rental_cost[:rental_cost_percentage_15_days] / 100 * params[:loan_value].to_i
        response_success(rental_cost, 'Hasil Perhitungan Rental Cost')
      rescue StandardError
        response_bad('Data Tidak DItemukan atau Parameter Salah')
      end
      # Only allow a trusted parameter "white list" through.

      private

      def check_params
        response_bad('Parameter Tidak Lengkap') unless params.key?(:insurance_item_id) && params.key?(:product_id) &&
                                                       params.key?(:loan_value)
      end

      def check_region
        @region_id = Area.find_by(code: current_owner.area_code).regional_id
      end
    end
  end
end
