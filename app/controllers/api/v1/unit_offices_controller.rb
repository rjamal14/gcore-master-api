module Api
  module V1
    # Class Kantor Unir
    class UnitOfficesController < ApplicationController
      before_action :set_unit_office, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /unit_offices
      def index
        unit_offices = UnitOffice.page(params[:page] || 1).per(params[:per_page] || 10)
                                 .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        unit_offices = params[:search] ? unit_offices.where(name: /.*#{params[:search]}.*/i) : unit_offices

        serial_unit_offices = unit_offices.map { |unit_office| UnitOfficeSerializer.new(unit_office, root: false) }
        response_index(unit_offices, serial_unit_offices)
      end

      def autocomplete
        @unit_offices = UnitOffice.only(:id, :name)
                                  .where(name: /.*#{params[:query]}.*/i)
                                  .order_by(name: :asc)
                                  .limit(10)
        response_success(@unit_offices, 'Data yang ditemukan')
      end

      # GET /unit_offices/1
      def show
        @unit_office = UnitOfficeDetailSerializer.new(@unit_office, root: false)
        response_success(@unit_office, 'Data Kantor Unit Ditemukan')
      end

      # POST /unit_offices
      def create
        @unit_office = UnitOffice.new(unit_office_params.merge(created_by_id: current_user))

        if @unit_office.save
          response_created(@unit_office, 'Data Kantor Unit tersimpan')
        else
          response_error(@unit_office, 'Data Kantor Unit gagal tersimpan')
        end
      end

      # PATCH/PUT /unit_offices/1
      def update
        if @unit_office.update(unit_office_params.merge(updated_by_id: current_user))
          response_updated(@unit_office, 'Data Kantor Unit terupdate')
        else
          response_error(@unit_office, 'Data Kantor Unit gagal teupdate')
        end
      end

      # DELETE /unit_offices/1
      def destroy
        if params.key?(:id)
          @unit_offices = UnitOffice.in(id: params[:id].split(','))
          if @unit_offices.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @unit_offices.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_unit_office
        @unit_office = UnitOffice.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def unit_office_params
        params.require(:unit_office).permit(:name, :code, :city_id, :branch_office_id, :description, :address, :latitude, :longitude)
      end
    end
  end
end
