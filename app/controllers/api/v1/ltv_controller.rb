# Ltv Controller
module Api
  module V1
    # Ltv Controller
    class LtvController < ApplicationController
      before_action :set_ltv, only: [:show, :update]

      # GET /ltv
      def index
        ltv = params[:search] ? Ltv.where(valid_date: params[:search], status: true) : Ltv

        ltv = ltv.page(params[:page] || 1).per(params[:per_page] || 10)
                 .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_ltv = ltv.map { |obj| LtvSerializer.new(obj, root: false) }

        response_index(ltv, serial_ltv)
      end

      # GET /ltv/1
      def show
        response_success(@ltv, 'Data Ditemukan')
      end

      # POST /ltv
      def create
        ltv = Ltv.new(ltv_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if ltv.save
          response_created(ltv, 'Data Ltv Tersimpan')
        else
          response_error(ltv, 'Data Ltv Gagal Tersimpan')
        end
      end

      # PATCH/PUT /ltv/1
      def update
        if @ltv.update(ltv_params.merge(updated_by_id: current_user))
          response_updated(@ltv, 'Data Ltv Terupdate')
        else
          response_error(@ltv, 'Data Ltv Gagal Terupdate')
        end
      end

      # DELETE /ltv/1
      def destroy
        if params.key?(:id)
          ltv = Ltv.in(id: params[:id].split(','))
          if ltv.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            ltv.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_ltv
        @ltv = Ltv.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def ltv_params
        params.require(:ltv).permit(:valid_date, :value_ltv)
      end
    end
  end
end
