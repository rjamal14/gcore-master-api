module Api
  module V1
    # Bank Controller
    class BanksController < ApplicationController
      before_action :set_bank, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /banks
      def index
        banks = params[:search] ? Bank.where(name: /.*#{params[:search]}.*/i) : Bank
        banks = banks.page(params[:page] || 1).per(params[:per_page] || 10)
                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        response_index(banks, banks)
      end

      def autocomplete
        banks = Bank.only(:id, :name)
                    .where(name: /.*#{params[:query]}.*/i, status: true)
                    .order_by(name: :asc)
                    .limit(10)
        response_success(banks, 'Data yang ditemukan')
      end

      # GET /banks/1
      def show
        response_success(@bank, 'Data Ditemukan')
      end

      # POST /banks
      def create
        bank = Bank.new(bank_params.merge(created_by_id: current_user))

        if bank.save
          response_created(bank, 'Data Bank Tersimpan')
        else
          response_error(bank, 'Data Bank gagal tersimpan')
        end
      end

      # PATCH/PUT /banks/1
      def update
        if @bank.update(bank_params.merge(updated_by_id: current_user))
          response_success(@bank, 'Data Bank terupdate')
        else
          response_error(@bank, 'Data Bank gagal terupdate')
        end
      end

      # DELETE /banks/1
      def destroy
        if params.key?(:id)
          banks = Bank.in(id: params[:id].split(','))
          if banks.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            banks.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_bank
        @bank = Bank.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def bank_params
        params.require(:bank).permit(:name, :status, :bank_code, :description)
      end
    end
  end
end
