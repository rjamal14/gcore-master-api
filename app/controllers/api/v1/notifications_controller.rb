# Notification Controller
module Api
  module V1
    # Notification Controller
    class NotificationsController < ApplicationController
      before_action :set_notification, only: :show
      before_action :set_notifications_owner, only: [:index, :unread_index]
      skip_before_action :current_owner, only: [:index, :show, :unread_index]
      # after_action :render_index, only: [:index, :unread_index]

      # GET /notifications
      def index
        @notifications = params[:search] ? @notifications.where(body: /.*#{params[:search]}.*/i) : @notifications
        render_index
      end

      def unread_index
        @notifications = @notifications.unread
        render_index
      end

      # GET /notifications/1
      def show
        notification_serializer = NotificationSerializer.new(@notification, root: false)
        @notification.update(is_read: true)
        response_success(notification_serializer, 'Data Ditemukan')
      end

      # DELETE /notifications/1
      def destroy
        if params.key?(:id)
          @notifications = Notification.in(id: params[:id].split(','))
          if @notifications.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @notifications.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      def render_index
        @notifications = @notifications.page(params[:page] || 1).per(params[:per_page] || 10)
                                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_notifications = @notifications.map { |notification| NotificationSerializer.new(notification, root: false) }

        response_index(@notifications, serial_notifications)
      end

      def set_notifications_owner
        owner = current_owner
        @notifications = Notification.where(offices: { office_id: owner.office_id, role: owner[:user_role][:role_name] })
      rescue StandardError
        true
      end

      def set_notification
        @notification = Notification.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def notification_params
        params.require(:notification).permit(:subject, :body, :url, :branch_office_id, :region_id, :area_id,
                                             :head_office_id, :unit_office_id)
      end
    end
  end
end
