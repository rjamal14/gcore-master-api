module Api
  module V1
    class InstallmentSimulationsController < ApplicationController
      skip_before_action :current_owner
      def simulate_installment
        installment = Transaction::SimulateInstallmentService.new(data: installment_params, office_id: params[:office_id],
                                                                  office_type: params[:office_type])
        installment = installment.execute(down_payment: installment_params[:down_payment], tenor: installment_params[:tenor])

        return response_bad('Uang muka kurang dari minimum') if installment == false

        response_success(installment, 'Data Simulasi Berhasil')
      rescue StandardError => e
        response_bad(e)
      end

      def check_installment_attribute
        installment_attribute = Transaction::InstallmentAttributeService.new(data: installment_params, office_id: params[:office_id],
                                                                             office_type: params[:office_type])
        response_success(installment_attribute.execute, 'Data Simulasi Berhasil')
      rescue StandardError => e
        response_bad(e)
      end

      private

      def installment_params
        params.require(:installment).permit(:product_id, :tenor, :insurance_item_id, :down_payment,
                                            insurance_items: [:name, :weight, :karats, :buy_price, :amount])
      end
    end
  end
end
