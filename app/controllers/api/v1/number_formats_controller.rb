# Class FOrmat Numebr
module Api
  module V1
    # Class Number Format
    class NumberFormatsController < ApplicationController
      before_action :set_number_format, only: [:show, :update]
      skip_before_action :current_owner, only: :generate_number
      before_action :validate_params, only: :generate_number

      # GET /number_formats
      def index
        @number_formats = con_num
        @number_formats = @number_formats.page(params[:page] || 1).per(params[:per_page] || 10)
                                         .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_number_formats = @number_formats.map { |number_formats| NumberFormatSerializer.new(number_formats, root: false) }

        response_index(@number_formats, serial_number_formats)
      end

      def con_num
        if params[:search_name] || params[:search_code]
          NumberFormat.where(name: /.*#{params[:search_name]}.*/i, code: /.*#{params[:search_code]}.*/i)
        else
          NumberFormat
        end
      end

      def autocomplete
        @number_formats = NumberFormat.only(:id, :name)
                                      .where(name: /.*#{params[:query]}.*/i)
                                      .order_by(name: :asc)
                                      .limit(10)
        response_success(@number_formats, 'Data yang ditemukan')
      end

      # GET /number_formats/1
      def show
        response_success(@number_format, 'Data Format Nomor Ditemukan')
      end

      # POST /number_formats
      def create
        @number_format = NumberFormat.new(number_format_params.merge(created_by_id: current_user))

        if @number_format.save
          response_created(@number_format, 'Data Format Nomor tersimpan')
        else
          response_error(@number_format, 'Data Format Nomor gagal tersimpan')
        end
      end

      # PATCH/PUT /number_formats/1
      def update
        if @number_format.update(number_format_params.merge(updated_by_id: current_user))
          response_updated(@number_format, 'Data Format Nomor terupdate')
        else
          response_error(@number_format, 'Data Format Nomor gagal teupdate')
        end
      end

      def generate_number
        number_generated = Transaction::GenerateNumberService.new(params[:office_id], params[:type], params[:office_type])
        @number_generated = number_generated.execute
        response_success(@number_generated, 'Nomor Berhasil di buat')
      end

      # DELETE /number_formats/1
      def destroy
        if params.key?(:id)
          @number_formats = NumberFormat.in(id: params[:id].split(','))
          if @number_formats.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            @number_formats.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_number_format
        @number_format = NumberFormat.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def validate_params
        if params.key?(:type) && (params[:type] == 'sge' || params[:type] == 'cif' || params[:type] == 'finance' || params[:type] == 'ref-petty' || params[:type] == 'ref-cash' || params[:type] == 'work-cap' || params[:type] == 'working-capital')
          return
        end

        response_bad('Parameter tidak lengkap')
      end

      # Only allow a trusted parameter "white list" through.
      def number_format_params
        params.require(:number_format).permit(:name, :code, :num_format, :year, :description, :last_number, :number_increment, :leading_zero)
      end
    end
  end
end
