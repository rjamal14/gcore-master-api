module Api
  module V1
    # Slte Controller
    class SlteController < ApplicationController
      before_action :set_slte, only: [:show, :update]

      # GET /slte
      def index
        slte = params[:search] ? Slte.where(valid_date: params[:search], status: true) : Slte
        slte = slte.page(params[:page] || 1).per(params[:per_page] || 10)
                   .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_slte = slte.map { |obj| SlteSerializer.new(obj, root: false) }
        response_index(slte, serial_slte)
      end

      # GET /slte/1
      def show
        response_success(@slte, 'Data Ditemukan')
      end

      # POST /slte
      def create
        slte = Slte.new(slte_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if slte.save
          response_created(slte, 'Data Slte Tersimpan')
        else
          response_error(slte, 'Data Slte Gagal Tersimpan')
        end
      end

      # PATCH/PUT /slte/1
      def update
        if @slte.update(slte_params.merge(updated_by_id: current_user.id))
          response_updated(@slte, 'Data Slte Terupdate')
        else
          response_error(@slte, 'Data Slte Gagal Terupdate')
        end
      end

      # DELETE /slte/1
      def destroy
        if params.key?(:id)
          slte = Slte.in(id: params[:id].split(','))
          if slte.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            slte.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_slte
        @slte = Slte.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def slte_params
        params.require(:slte).permit(:valid_date, :value_slte)
      end
    end
  end
end
