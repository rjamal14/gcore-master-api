# ProductOneObligorController
module Api
  module V1
    # ProductOneObligorController
    class ProductOneObligorController < ApplicationController
      before_action :set_product_one_obligor, only: [:show, :update, :destroy]
      def index
        @product_one_obligor = if params[:search]
                                 ProductOneObligor.where(product_id: params[:search])
                               else
                                 ProductOneObligor
                               end

        @product_one_obligor = @product_one_obligor.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_one_obligor = @product_one_obligor.map { |product_one_obligor| ProductOneObligorSerializer.new(product_one_obligor, root: false) }

        response_index(@product_one_obligor, serial_product_one_obligor)
      end

      # GET /product_one_obligor/1
      def show
        response_success(@product_one_obligor, 'Data Ditemukan')
      end

      # POST /product_one_obligor
      def create
        @product_one_obligor = ProductOneObligor.new(product_one_obligor_params.merge(product_id: params[:product_id]))

        if @product_one_obligor.save
          response_created(@product_one_obligor, 'Data ProductOneObligor Tersimpan')
        else
          response_error(@product_one_obligor, 'Data ProductOneObligor Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_one_obligor/1
      def update
        if @product_one_obligor.update(product_one_obligor_params)
          response_updated(@product_one_obligor, 'Data ProductOneObligor Terupdate')
        else
          response_error(@product_one_obligor, 'Data ProductOneObligor Gagal Terupdate')
        end
      end

      # DELETE /product_one_obligor/1
      def destroy
        @product_one_obligor.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_one_obligor
        @product_one_obligor = ProductOneObligor.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_one_obligor_params
        params.require(:product_one_obligor).permit(:limit_name,
                                                    limit_item: [:item_name, :item_value])
      end
    end
  end
end
