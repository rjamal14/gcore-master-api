module Api
  module V1
    # Class Verifikasi Item
    class VerificationItemsController < ApplicationController
      before_action :set_verification_item, only: [:show, :update]

      # GET /verification_items
      def index
        verification_items = params[:search] ? VerificationItem.where(name: /.*#{params[:search]}.*/i) : VerificationItem
        verification_items = verification_items.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_verification_items = verification_items.map { |verification_item| VerificationItemSerializer.new(verification_item, root: false) }
        response_index(verification_items, serial_verification_items)
      end

      def autocomplete
        verification_items = VerificationItem.only(:id, :name)
                                             .where(name: /.*#{params[:query]}.*/i)
                                             .order_by(name: :asc)
                                             .limit(10)
        response_success(verification_items, 'Data yang ditemukan')
      end

      # GET /verification_items/1
      def show
        response_success(@verification_item, 'Data Verifikasi Item Ditemukan')
      end

      # POST /verification_items
      def create
        verification_item = VerificationItem.new(verification_item_params.merge(created_by_id: current_user))

        if verification_item.save
          response_created(verification_item, 'Data Verifikasi Item tersimpan')
        else
          response_error(verification_item, 'Data Verifikasi Item gagal tersimpan')
        end
      end

      # PATCH/PUT /verification_items/1
      def update
        if @verification_item.update(verification_item_params.merge(updated_by_id: current_user))
          response_updated(@verification_item, 'Data Verivikasi Item terupdate')
        else
          response_error(@verification_item, 'Data Verifikasi Item gagal teupdate')
        end
      end

      # DELETE /verification_items/1
      def destroy
        if params.key?(:id)
          verification_items = VerificationItem.in(id: params[:id].split(','))
          if verification_items.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            verification_items.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_verification_item
        @verification_item = VerificationItem.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def verification_item_params
        params.require(:verification_item).permit(:name, :status, :document_needed, verification_type_ids: [:verification_type_id])
      end
    end
  end
end
