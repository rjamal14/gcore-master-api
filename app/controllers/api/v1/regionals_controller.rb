module Api
  module V1
    # Class Regional
    class RegionalsController < ApplicationController
      before_action :set_regional, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /regionals
      def index
        regionals = params[:search] ? Regional.where(name: /.*#{params[:search]}.*/i) : Regional
        regionals = regionals.page(params[:page] || 1).per(params[:per_page] || 10)
                             .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_regionals = regionals.map { |regional| RegionalSerializer.new(regional, root: false) }
        response_index(regionals, serial_regionals)
      end

      def autocomplete
        regionals = Regional.only(:id, :name)
                            .where(name: /.*#{params[:query]}.*/i)
                            .order_by(name: :asc)
                            .limit(10)
        if regionals.present?
          response_success(regionals, 'Berhasil')
        else
          response_success(regionals, 'data kosong / tidak ditemukan')
        end
      end

      # GET /regionals/1
      def show
        @regional = RegionOfficeDetailSerializer.new(@regional, root: false)
        response_success(@regional, 'Data Regional Ditemukan')
      end

      # POST /regionals
      def create
        regional = Regional.new(regional_params.merge(created_by_id: current_user))

        if regional.save
          response_created(regional, 'Data regional berhasil tersimpan')
        else
          response_error(regional, 'Data regional gagal tersimpan')
        end
      end

      # PATCH/PUT /regionals/1
      def update
        if @regional.update(regional_params.merge(created_by_id: current_user))
          response_updated(@regional, 'Data regional terupdate')
        else
          response_error(@regional, 'Data regional gagal terupdate')
        end
      end

      # DELETE /regionals/1
      def destroy
        if params.key?(:id)
          regionals = Regional.in(id: params[:id].split(','))
          if regionals.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            regionals.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_regional
        @regional = Regional.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def regional_params
        params.require(:regional).permit(:name, :code, :city_id, :address, :latitude, :longitude, :description, :head_office_id, :logo, :company_name)
      end
    end
  end
end
