module Api
  module V1
    # ServiceFee Controller
    class ServiceFeeController < ApplicationController
      before_action :set_service_fee, only: [:show, :update]

      # GET /service_fee
      def index
        service_fees = params[:search] ? ServiceFee.where(valid_date: params[:search], status: true) : ServiceFee
        service_fees = service_fees.page(params[:page] || 1).per(params[:per_page] || 10)
                                   .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_service_fees = service_fees.map { |service_fee| ServiceFeeSerializer.new(service_fee, root: false) }
        response_index(service_fees, serial_service_fees)
      end

      # GET /service_fee/1
      def show
        response_success(@service_fee, 'Data Ditemukan')
      end

      # POST /service_fee
      def create
        service_fee = ServiceFee.new(service_fee_params.merge(created_by_id: current_user, updated_by_id: current_user))

        if service_fee.save
          response_created(service_fee, 'Data ServiceFee Tersimpan')
        else
          response_error(service_fee, 'Data ServiceFee Gagal Tersimpan')
        end
      end

      # PATCH/PUT /service_fee/1
      def update
        if @service_fee.update(service_fee_params.merge(updated_by_id: current_user))
          response_updated(@service_fee, 'Data ServiceFee Terupdate')
        else
          response_error(@service_fee, 'Data ServiceFee Gagal Terupdate')
        end
      end

      # DELETE /service_fee/1
      def destroy
        if params.key?(:id)
          service_fees = ServiceFee.in(id: params[:id].split(','))
          if service_fees.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            service_fees.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_service_fee
        @service_fee = ServiceFee.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def service_fee_params
        params.require(:service_fee).permit(:valid_date, :value_fee)
      end
    end
  end
end
