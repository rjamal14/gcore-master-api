# Controller Kantor Cabang
module Api
  module V1
    # Controller Kantor Cabang
    class BranchOfficesController < ApplicationController
      before_action :set_branch_office, only: [:show, :update]
      skip_before_action :current_owner, only: :autocomplete

      # GET /branch_offices
      def index
        branch_offices = params[:search] ? BranchOffice.where(name: /.*#{params[:search]}.*/i) : BranchOffice
        branch_offices = branch_offices.page(params[:page] || 1).per(params[:per_page] || 10)
                                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_branch_offices = branch_offices.map { |branch_office| BranchOfficeSerializer.new(branch_office, root: false) }

        response_index(branch_offices, serial_branch_offices)
      end

      def autocomplete
        branch_offices = BranchOffice.only(:id, :name, :city_id)
                                     .where(name: /.*#{params[:query]}.*/i)
                                     .order_by(name: :asc)
                                     .limit(10)
        response_success(branch_offices, 'Data yang ditemukan')
      end

      # GET /branch_offices/1
      def show
        @branch_office = BranchOfficeDetailSerializer.new(@branch_office, root: false)
        if @branch_office.present?
          response_success(@branch_office, 'Data Ditemukan')
        else
          response_bad('Data Tidak Ditemukan')
        end
      end

      # POST /branch_offices
      def create
        @branch_office = BranchOffice.new(branch_office_params.merge(created_by_id: current_user))

        if @branch_office.save
          response_created(@branch_office, 'Data kantor cabang tersimpan')
        else
          response_error(@branch_office, 'Data kantor cabang gagal tersimpan')
        end
      end

      # PATCH/PUT /branch_offices/1
      def update
        if @branch_office.update(branch_office_params.merge(updated_by_id: current_user))
          response_updated(@branch_office, 'Data kantor cabang terupdate')
        else
          response_error(@branch_office, 'Data kantor cabang gagal terupdate')
        end
      end

      # DELETE /branch_offices/1
      def destroy
        if params.key?(:id)
          branch_offices = BranchOffice.in(id: params[:id].split(','))
          if branch_offices.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            branch_offices.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_branch_office
        @branch_office = BranchOffice.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def branch_office_params
        params.require(:branch_office).permit(:name, :code, :city_id, :area_id, :address, :latitude, :longitude, :description)
      end
    end
  end
end
