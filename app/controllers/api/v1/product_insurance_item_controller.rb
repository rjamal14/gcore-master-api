# ProductInsuranceItemController
module Api
  module V1
    # ProductInsuranceItemController
    class ProductInsuranceItemController < ApplicationController
      before_action :set_product_insurance_item, only: [:show, :update, :destroy]
      before_action :check_params_autocomplete, only: [:autocomplete]
      def index
        @product_insurance_item = if params[:search]
                                    ProductInsuranceItem.where(product_id: params[:search])
                                  else
                                    ProductInsuranceItem
                                  end

        @product_insurance_item = @product_insurance_item.page(params[:page] || 1).per(params[:per_page] || 10)
                                                         .order_by(created_at: :asc)

        serial_product_insurance_item = @product_insurance_item.map do |product_insurance_item|
          ProductInsuranceItemSerializer.new(product_insurance_item, root: false)
        end

        response_index(@product_insurance_item, serial_product_insurance_item)
      end

      def autocomplete
        @product_insurance_item = ProductInsuranceItem.where(name: /.*#{params[:name]}.*/i, product_id: params[:product_id],
                                                             insurance_item_id: params[:insurance_item_id])
                                                      .order_by(name: :asc)
                                                      .limit(10)
        if @product_insurance_item.present?
          serial_product_insurance_item = @product_insurance_item.map do |product_insurance_item|
            ProductInsuranceItemSerializer.new(product_insurance_item, root: false)
          end
          response_success(serial_product_insurance_item, 'Berhasil')
        else
          response_bad('data kosong / tidak ditemukan')
        end
      end

      # GET /product_insurance_item/1
      def show
        response_success(@product_insurance_item, 'Data Ditemukan')
      end

      # POST /product_insurance_item
      def create
        @product_insurance_item = ProductInsuranceItem.new(product_insurance_item_params.merge(product_id: params[:product_id]))

        if @product_insurance_item.save
          response_created(@product_insurance_item, 'Data ProductInsuranceItem Tersimpan')
        else
          response_error(@product_insurance_item, 'Data ProductInsuranceItem Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_insurance_item/1
      def update
        if @product_insurance_item.update(product_insurance_item_params)
          response_updated(@product_insurance_item, 'Data ProductInsuranceItem Terupdate')
        else
          response_error(@product_insurance_item, 'Data ProductInsuranceItem Gagal Terupdate')
        end
      end

      # DELETE /product_insurance_item/1
      def destroy
        @product_insurance_item.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_insurance_item
        @product_insurance_item = ProductInsuranceItem.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def check_params_autocomplete
        response_bad('Paramater Tidak Lengkap') unless params.key?(:product_id) && params.key?(:insurance_item_id)
      end

      # Only allow a trusted parameter "white list" through.
      def product_insurance_item_params
        params.require(:product_insurance_item).permit(:insurance_item_id, :name, :minimum_karatase, :certificate)
      end
    end
  end
end
