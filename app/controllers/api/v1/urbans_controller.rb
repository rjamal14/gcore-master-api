# Urban Controller
module Api
  module V1
    # Urban Controller
    class UrbansController < ApplicationController
      before_action :set_urban, only: [:show, :update]
      before_action :set_params, only: [:autocomplete, :index]
      skip_before_action :current_owner, only: :autocomplete

      # GET /urbans
      def index
        urbans = params[:search] ? Urban.where(name: /.*#{params[:search]}.*/i) : Urban
        urbans = urbans.page(params[:page] || 1).per(params[:per_page] || 10)
                       .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_urbans = urbans.map { |urban| UrbanSerializer.new(urban, root: false) }

        response_index(urbans, serial_urbans)
      end

      def autocomplete
        urbans = if params.key?(:district_id)
                   Urban.where(name: /.*#{params[:query]}.*/i, district_id: params[:district_id])
                 else
                   Urban.where(name: /.*#{params[:query]}.*/i)
                 end
        urbans = urbans.only(:id, :name, :district_id, :postal_code)
                       .order_by(name: :asc)
                       .limit(10)
        if urbans.present?
          response_success(urbans, 'Berhasil')
        else
          response_success(urbans, 'data kosong / tidak ditemukan')
        end
      end

      # GET /urbans/1
      def show
        urban_serializer = UrbanSerializer.new(@urban)
        response_success(urban_serializer, 'Data Ditemukan')
      end

      # POST /urbans
      def create
        urban = Urban.new(urban_params.merge(created_by_id: current_user))

        if urban.save
          response_created(urban, 'Data kecamatan Tersimpan')
        else
          response_error(urban, 'Data kecamatan Gagal Tersimpan')
        end
      end

      # PATCH/PUT /urbans/1
      def update
        if @urban.update(urban_params.merge(updated_by_id: current_user))
          response_updated(@urban, 'Data kecamatan Terupdate')
        else
          response_error(@urban, 'Data kecamatan Gagal Terupdate')
        end
      end

      # DELETE /urbans/1
      def destroy
        if params.key?(:id)
          urbans = Urban.in(id: params[:id].split(','))
          if urbans.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            urbans.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      def set_params
        params[:district_id] = params[:district_id].to_i if params.key?(:district_id)
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_urban
        @urban = Urban.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def urban_params
        params.require(:urban).permit(:name, :district_id, :postal_code)
      end
    end
  end
end
