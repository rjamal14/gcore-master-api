# ProductAdminCostController
module Api
  module V1
    # ProductAdminCostController
    class ProductAdminCostController < ApplicationController
      before_action :set_product_admin_cost, only: [:show, :update, :destroy]
      def index
        @product_admin_cost = if params[:search]
                                ProductAdminCost.where(product_id: params[:search])
                              else
                                ProductAdminCost
                              end

        @product_admin_cost = @product_admin_cost.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_admin_cost = @product_admin_cost.map { |product_admin_cost| ProductAdminCostSerializer.new(product_admin_cost, root: false) }

        response_index(@product_admin_cost, serial_product_admin_cost)
      end

      # GET /product_admin_cost/1
      def show
        response_success(@product_admin_cost, 'Data Ditemukan')
      end

      # POST /product_admin_cost
      def create
        @product_admin_cost = ProductAdminCost.new(product_admin_cost_params.merge(product_id: params[:product_id]))

        if @product_admin_cost.save
          response_created(@product_admin_cost, 'Data ProductAdminCost Tersimpan')
        else
          response_error(@product_admin_cost, 'Data ProductAdminCost Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_admin_cost/1
      def update
        if @product_admin_cost.update(product_admin_cost_params)
          response_updated(@product_admin_cost, 'Data ProductAdminCost Terupdate')
        else
          response_error(@product_admin_cost, 'Data ProductAdminCost Gagal Terupdate')
        end
      end

      # DELETE /product_admin_cost/1
      def destroy
        @product_admin_cost.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_admin_cost
        @product_admin_cost = ProductAdminCost.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_admin_cost_params
        params.require(:product_admin_cost).permit(:min_loan_range, :max_loan_range, :cost_nominal, :cost_percentage, :insurance_item_id)
      end
    end
  end
end
