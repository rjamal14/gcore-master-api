# ProductRentalCostDiscountController
module Api
  module V1
    # Product Reantal COst Controller
    class ProductRentalCostDiscountController < ApplicationController
      before_action :set_product_rental_cost_discount, only: [:show, :update, :destroy]
      def index
        @product_rental_cost_discount = if params[:search]
                                          ProductRentalCostDiscount.where(product_id: params[:search])
                                        else
                                          ProductRentalCostDiscount
                                        end

        @product_rental_cost_discount = @product_rental_cost_discount.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_rental_cost_discount = @product_rental_cost_discount.map { |product_rental_cost_discount| ProductRentalCostDiscountSerializer.new(product_rental_cost_discount, root: false) }

        response_index(@product_rental_cost_discount, serial_product_rental_cost_discount)
      end

      # GET /product_rental_cost_discount/1
      def show
        response_success(@product_rental_cost_discount, 'Data Ditemukan')
      end

      # POST /product_rental_cost_discount
      def create
        @product_rental_cost_discount = ProductRentalCostDiscount.new(product_rental_cost_discount_params.merge(product_id: params[:product_id]))

        if @product_rental_cost_discount.save
          response_created(@product_rental_cost_discount, 'Data ProductRentalCostDiscount Tersimpan')
        else
          response_error(@product_rental_cost_discount, 'Data ProductRentalCostDiscount Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_rental_cost_discount/1
      def update
        if @product_rental_cost_discount.update(product_rental_cost_discount_params)
          response_updated(@product_rental_cost_discount, 'Data ProductRentalCostDiscount Terupdate')
        else
          response_error(@product_rental_cost_discount, 'Data ProductRentalCostDiscount Gagal Terupdate')
        end
      end

      # DELETE /product_rental_cost_discount/1
      def destroy
        @product_rental_cost_discount.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_rental_cost_discount
        @product_rental_cost_discount = ProductRentalCostDiscount.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_rental_cost_discount_params
        params.require(:product_rental_cost_discount).permit(
          :repawn_1, :repawn_2, :repawn_3,
          :repawn_4, :repawn_5, :repawn_6,
          :repawn_7, :repawn_8, :repawn_9,
          :repawn_10, :repawn_11, :repawn_12, :insurance_item_id
        )
      end
    end
  end
end
