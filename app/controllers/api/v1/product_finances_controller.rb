# Product FInance COntroller
module Api
  module V1
    # Controller
    class ProductFinancesController < ApplicationController
      before_action :set_product_finance, only: [:show, :update, :destroy]

      # GET /product_finances
      def index
        @product_finances = if params[:search]
                              ProductFinance.where(name: /.*#{params[:search]}.*/i)
                            else
                              ProductFinance
                            end
        @product_finances = @product_finances.page(params[:page] || 1).per(params[:per_page] || 10)
        serial_product_finances = @product_finances.map { |product_finance| ProductFinanceSerializer.new(product_finance, root: false) }
        response_index(@product_finances, serial_product_finances)
      end

      def autocomplete
        @product_finances = ProductFinance.only(:id, :name, :country_id)
                                          .where(name: /.*#{params[:query]}.*/i)
                                          .order_by(name: :asc)
                                          .limit(10)
        if @product_finances.present?
          response_success(@product_finances, 'Berhasil')
        else
          response_success(@product_finances, 'data kosong / tidak ditemukan')
        end
      end

      # GET /product_finances/1
      def show
        response_success(@product_finance, 'Data produk pembiayaan ditemukan')
      end

      # POST /product_finances
      def create
        @product_finance = ProductFinance.create(product_finance_params)

        if @product_finance.save
          response_created(@product_finance, 'Data produk pembiayaan tersimpan')
        else
          response_error(@product_finance, 'Data produk pembiayaan gagal tersimpan')
        end
      end

      # PATCH/PUT /product_finances/1
      def update
        if @product_finance.update(product_finance_params)
          response_updated(@product_finance, 'Date produk pembiayaan terupdate')
        else
          response_error(@product_finance, 'Data produk pembiayaan gagal terupdate')
        end
      end

      # DELETE /product_finances/1
      def destroy
        response_ok('Data Terhapus') if @product_finance.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_finance
        @product_finance = ProductFinance.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_finance_params
        params.require(:product_finance).permit(:product_id, :insurance_item_id, :min_karatase, :min_loan, :max_loan, :min_weight,
                                                product_finance_items_attributes: [:id, :regional_id, :ltv_value, :stle_value, :_destroy])
      end
    end
  end
end
