module Api
  module V1
    class OfficesController < ApplicationController
      before_action :office_setter, only: [:show]
      skip_before_action :current_owner, only: [:show]

      def show
        if @office.present?
          render json: @office, office_type: params[:office_type], serializer: OfficesSerializer
        else
          response_bad('Data Tidak Ditemukan')
        end
      end

      private

      def office_setter
        @office = find_office_by_type
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def find_office_by_type
        case params[:office_type]
        when 'unit'
          UnitOffice.find(params[:id])
        when 'branch'
          BranchOffice.find(params[:id])
        when 'area'
          Area.find(params[:id])
        when 'region'
          Regional.find(params[:id])
        when 'head'
          HeadOffice.find(params[:id])
        end
      end
    end
  end
end
