module Api
  module V1
    # Bank Controller
    class GoldPricesController < ApplicationController
      def show; end

      # Only allow a trusted parameter "white list" through.
      def fetch_by_params
        check_params
        response_success(@gold_prices, 'Data Ditemukan')
      end

      def check_unit
        @gold_prices = GoldPrice.where(created_at: Time.parse(params[:start])..Time.parse(params[:end]))
      rescue StandardError
        response_bad('Penulisan Tanggal salah/ Kurang lengkap')
      else
        assignment
      end

      def check_params
        if params.key?(:unit) && check
          check_unit
        elsif check && !params.key?(:unit)
          @gold_prices = GoldPrice.where(created_at: Time.parse(params[:start])..Time.parse(params[:end]))
        else
          @gold_prices = GoldPrice.last
        end
      end

      def check
        params.key?(:start) && params.key?(:end)
      end

      def assignment
        @gold_prices = case params[:unit]
                       when 'kg'
                         @gold_prices.only(:kilogram, :created_at)
                       when 'gram'
                         @gold_prices.only(:gram, :created_at)
                       when 'ounce'
                         @gold_prices.only(:ounce, :created_at)
                       else
                         @gold_prices
                       end
      end
    end
  end
end
