# ProductDeviationSetupController
module Api
  module V1
    # ProductDeviationSetupController
    class ProductDeviationSetupController < ApplicationController
      before_action :set_product_deviation_setup, only: [:show, :update, :destroy]
      def index
        @product_deviation_setup = if params[:search]
                                     ProductDeviationSetup.where(product_id: params[:search])
                                   else
                                     ProductDeviationSetup
                                   end

        @product_deviation_setup = @product_deviation_setup.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_deviation_setup = @product_deviation_setup.map { |product_deviation_setup| ProductDeviationSetupSerializer.new(product_deviation_setup, root: false) }

        response_index(@product_deviation_setup, serial_product_deviation_setup)
      end

      # GET /product_deviation_setup/1
      def show
        response_success(@product_deviation_setup, 'Data Ditemukan')
      end

      # POST /product_deviation_setup
      def create
        @product_deviation_setup = ProductDeviationSetup.new(product_deviation_setup_params.merge(product_id: params[:product_id]))

        if @product_deviation_setup.save
          response_created(@product_deviation_setup, 'Data ProductDeviationSetup Tersimpan')
        else
          response_error(@product_deviation_setup, 'Data ProductDeviationSetup Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_deviation_setup/1
      def update
        if @product_deviation_setup.update(product_deviation_setup_params)
          response_updated(@product_deviation_setup, 'Data ProductDeviationSetup Terupdate')
        else
          response_error(@product_deviation_setup, 'Data ProductDeviationSetup Gagal Terupdate')
        end
      end

      # DELETE /product_deviation_setup/1
      def destroy
        @product_deviation_setup.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_deviation_setup
        @product_deviation_setup = ProductDeviationSetup.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_deviation_setup_params
        params.require(:product_deviation_setup).permit(:name, :insurance_item_id, :branch, :area, :region, :headquarters)
      end
    end
  end
end
