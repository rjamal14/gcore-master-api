# ProductAdminCostDiscountController
module Api
  module V1
    # ProductAdminCostDiscountController
    class ProductAdminCostDiscountController < ApplicationController
      before_action :set_product_admin_cost_discount, only: [:show, :update, :destroy]
      def index
        @product_admin_cost_discount = if params[:search]
                                         ProductAdminCostDiscount.where(product_id: params[:search])
                                       else
                                         ProductAdminCostDiscount
                                       end

        @product_admin_cost_discount = @product_admin_cost_discount.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_admin_cost_discount = @product_admin_cost_discount.map { |product_admin_cost_discount| ProductAdminCostDiscountSerializer.new(product_admin_cost_discount, root: false) }

        response_index(@product_admin_cost_discount, serial_product_admin_cost_discount)
      end

      # GET /product_admin_cost_discount/1
      def show
        response_success(@product_admin_cost_discount, 'Data Ditemukan')
      end

      # POST /product_admin_cost_discount
      def create
        @product_admin_cost_discount = ProductAdminCostDiscount.new(product_admin_cost_discount_params.merge(product_id: params[:product_id]))

        if @product_admin_cost_discount.save
          response_created(@product_admin_cost_discount, 'Data ProductAdminCostDiscount Tersimpan')
        else
          response_error(@product_admin_cost_discount, 'Data ProductAdminCostDiscount Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_admin_cost_discount/1
      def update
        if @product_admin_cost_discount.update(product_admin_cost_discount_params)
          response_updated(@product_admin_cost_discount, 'Data ProductAdminCostDiscount Terupdate')
        else
          response_error(@product_admin_cost_discount, 'Data ProductAdminCostDiscount Gagal Terupdate')
        end
      end

      # DELETE /product_admin_cost_discount/1
      def destroy
        @product_admin_cost_discount.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_admin_cost_discount
        @product_admin_cost_discount = ProductAdminCostDiscount.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_admin_cost_discount_params
        params.require(:product_admin_cost_discount).permit(
          :repawn_1, :repawn_2, :repawn_3,
          :repawn_4, :repawn_5, :repawn_6,
          :repawn_7, :repawn_8, :repawn_9,
          :repawn_10, :repawn_11, :repawn_12, :insurance_item_id
        )
      end
    end
  end
end
