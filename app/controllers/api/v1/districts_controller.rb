# Kelurahan Controller
module Api
  module V1
    # Kelurahan Controller
    class DistrictsController < ApplicationController
      before_action :set_district, only: [:show, :update]
      before_action :set_params, only: [:index, :autocomplete]
      skip_before_action :current_owner, only: :autocomplete

      # GET /districts
      def index
        districts = params[:search] ? District.where(name: /.*#{params[:search]}.*/i) : District
        districts = districts.page(params[:page] || 1).per(params[:per_page] || 10)
                             .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_districts = districts.map { |district| DistrictSerializer.new(district, root: false) }

        response_index(districts, serial_districts)
      end

      def autocomplete
        @districts = if params.key?(:city_id)
                       District.where(name: /.*#{params[:query]}.*/i, city_id: params[:city_id])
                     else
                       District.where(name: /.*#{params[:query]}.*/i)
                     end
        @districts = @districts.only(:id, :name, :city_id, :district_code)
                               .order_by(name: :asc)
                               .limit(10)
        if @districts.present?
          response_success(@districts, 'Berhasil')
        else
          response_success(@districts, 'data kosong / tidak ditemukan')
        end
      end

      # GET /districts/1
      def show
        district_serializer = DistrictSerializer.new(@district)
        response_success(district_serializer, 'Data Ditemukan')
      end

      # POST /districts
      def create
        district = District.new(district_params.merge(created_by_id: current_user))
        district.city_id = setter_city_id(district.city_id)

        if district.save
          response_created(district, 'Data Kelurahan Tersimpan')
        else
          response_error(district, 'Data Kelurahan Gagal Tersimpan')
        end
      end

      # PATCH/PUT /districts/1
      def update
        if @district.update(district_params.merge(updated_by_id: current_user))
          response_updated(@district, 'Data Kelurahan Terupdate')
        else
          response_error(@district, 'Data Kelurahan Gagal Terupdate')
        end
      end

      # DELETE /districts/1
      def destroy
        if params.key?(:id)
          districts = District.in(id: params[:id].split(','))
          if districts.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            districts.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def setter_city_id(attr_city)
        city = City.where(city_code: attr_city).last
        if city.present?
          city.city_code
        else
          City.where(id: attr_city).last.city_code
        end
      end

      def set_district
        @district = District.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def set_params
        params[:city_id] = params[:city_id].to_i if params.key?(:city_id)
      end

      # Only allow a trusted parameter "white list" through.
      def district_params
        params.require(:district).permit(:name, :city_id)
      end
    end
  end
end
