# money_bill Controller
module Api
  module V1
    # money_bill Controller
    class MoneyBillsController < ApplicationController
      before_action :set_money_bill, only: [:show, :update]

      # GET /money_bills
      def index
        money_bills = MoneyBill.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        money_bills = params[:search] ? money_bills.where(description: /.*#{params[:search]}.*/i) : money_bills
        serial_money_bills = money_bills.map { |money_bill| MoneyBillSerializer.new(money_bill, root: false) }

        response_index(money_bills, serial_money_bills)
      end

      def list_all_money_bills
        @money_bills = MoneyBill.where(status: true).map { |money_bill| MoneyBillSerializer.new(money_bill, root: false) }
        response_success(@money_bills, 'Data Pecahan Uang')
      end

      def autocomplete
        @money_bills = MoneyBill.only(:id, :nominal)
                                .where(description: /.*#{params[:query]}.*/i)
                                .order_by(created_at: :asc)
                                .limit(10)
        if @money_bills.present?
          response_success(@money_bills, 'Berhasil')
        else
          response_success(@money_bills, 'data kosong / tidak ditemukan')
        end
      end

      # GET /money_bills/1
      def show
        @money_bill = MoneyBillSerializer.new(@money_bill, root: false)
        response_success(@money_bill, 'Data Pecahan Uang Ditemukan')
      end

      # POST /money_bills
      def create
        @money_bill = MoneyBill.new(money_bill_params.merge(created_by_id: current_user))

        if @money_bill.save
          response_created(@money_bill, 'Data Pecahan Uang Tersimpan')
        else
          response_error(@money_bill, 'Data Pecahan Uang Gagal Tersimpan')
        end
      end

      # PATCH/PUT /money_bills/1
      def update
        if @money_bill.update(money_bill_params.merge(updated_by_id: current_user))
          response_updated(@money_bill, 'Data Pecahan Uang Terupdate')
        else
          response_error(@money_bill, 'Data Pecahan Uang Gagal Terupdate')
        end
      end

      # DELETE /money_bills/1
      def destroy
        if params.key?(:id)
          @money_bills = MoneyBill.in(id: params[:id].split(','))
          if @money_bills.first.nil?
            response_not_found('Data Pecahan Uang Tidak ditemukan')
          else
            @money_bills.destroy_all
            response_ok('Data Pecahan Uang terhapus')
          end
        else
          response_bad('Gagal Pecahan Uang Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_money_bill
        @money_bill = MoneyBill.find(params[:id])
      rescue StandardError
        response_not_found('Data Pecahan Uang Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def money_bill_params
        params.require(:money_bill).permit(:nominal, :money_type, :status, :description)
      end
    end
  end
end
