module Api
  module V1
    # Province Controller
    class ProvincesController < ApplicationController
      before_action :set_province, only: [:show, :update, :master_data]
      skip_before_action :current_owner, only: [:autocomplete, :autocomplete_by_country]

      # GET /provinces
      def index
        provinces = params[:search] ? Province.where(name: /.*#{params[:search]}.*/i) : Province
        provinces = provinces.page(params[:page] || 1).per(params[:per_page] || 10)
                             .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        serial_provinces = provinces.map { |province| ProvinceSerializer.new(province, root: false) }
        response_index(provinces, serial_provinces)
      end

      def index_by_country
        if params.key?(:country_id)
          provinces = Province.where(country_id: params[:country_id]).only(:id, :name, :province_code)
          if provinces.blank?
            response_bad('Data kosong')
          else
            response_success(provinces, 'Data provinsi ditemukan')
          end
        else
          response_bad('Parameter tidak lengkap')
        end
      end

      def autocomplete
        provinces = Province.only(:id, :name, :country_id, :province_code)
                            .where(name: /.*#{params[:query]}.*/i)
                            .order_by(name: :asc)
                            .limit(10)
        if provinces.present?
          response_success(provinces, 'Berhasil')
        else
          response_success(provinces, 'data kosong / tidak ditemukan')
        end
      end

      def autocomplete_by_country
        provinces = Province.where(country_id: params[:country])
                            .where(name: /.*#{params[:query]}.*/i)
                            .order_by(name: :asc)
                            .limit(10)
        if provinces.present?
          response_success(provinces, 'Berhasil')
        else
          response_success(provinces, 'data kosong / tidak ditemukan')
        end
      end

      # GET /provinces/1
      def show
        response_success(ProvinceSerializer.new(@province, root: false), 'Data provinsi ditemukan')
      end

      def master_data
        serializer = ShowProvinceSerializer.new(@province, root: false)
        response_success(serializer, 'Data provinsi ditemukan')
      end

      # POST /provinces
      def create
        province = Province.new(province_params.merge(created_by_id: current_user))

        if province.save
          response_created(province, 'Data provinsi tersimpan')
        else
          response_error(province, 'Data provinsi gagal tersimpan')
        end
      end

      # PATCH/PUT /provinces/1
      def update
        if @province.update(province_params.merge(updated_by_id: current_user))
          response_updated(@province, 'Date provinsi terupdate')
        else
          response_error(@province, 'Data provinsi gagal terupdate')
        end
      end

      # DELETE /provinces/1
      def destroy
        if params.key?(:id)
          provinces = Province.in(id: params[:id].split(','))
          if provinces.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            provinces.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_province
        @province = Province.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def province_params
        params.require(:province).permit(:name, :country_id, :province_code)
      end
    end
  end
end
