module Api
  module V1
    # Admin Fees COntroller
    class TransactionCostsController < ApplicationController
      def transaction_cost_getter
        transaction_cost = Transaction::TransactionCostService.new(params)
        response_success(transaction_cost.execute, 'Data Ditemukan')
      rescue StandardError => e
        response_bad(e)
      end
    end
  end
end
