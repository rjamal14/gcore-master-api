module Api
  module V1
    # Bank Controller
    class DueDatesController < ApplicationController
      before_action :set_date, only: [:show_date]
      def show_date
        due_date = {}
        due_date[:due_date] = Date.parse(params[:contract_date]) + @date[:due_date] + @date[:period_of_time]
        due_date[:grace_period] = due_date[:due_date] + @date[:grace_period]
        due_date[:auction_date] = due_date[:grace_period] + @date[:auction_period]
        response_success(due_date, 'Tanggal Jatuh tempo dan Lelang')
      end

      def set_date
        if params.key?(:product_id) && params.key?(:contract_date)
          @date = ProductPeriod.find_by(product_id: params[:product_id])
        else
          response_bad('Paramater tidak lengkap')
        end
      rescue StandardError
        response_not_found('data tidak ditemukan')
      end
    end
  end
end
