# ProductPeriodController
module Api
  module V1
    # Controller Product Period
    class ProductPeriodController < ApplicationController
      before_action :set_product_period, only: [:show, :update, :destroy]
      def index
        @product_period = if params[:search]
                            ProductPeriod.where(product_id: params[:search])
                          else
                            ProductPeriod
                          end

        @product_period = @product_period.page(params[:page] || 1).per(params[:per_page] || 10).order_by(created_at: :asc)

        serial_product_period = @product_period.map { |product_period| ProductPeriodSerializer.new(product_period, root: false) }

        response_index(@product_period, serial_product_period)
      end

      # GET /product_period/1
      def show
        response_success(@product_period, 'Data Ditemukan')
      end

      # POST /product_period
      def create
        @product_period = ProductPeriod.new(product_period_params.merge(product_id: params[:product_id]))

        if @product_period.save
          response_created(@product_period, 'Data ProductPeriod Tersimpan')
        else
          response_error(@product_period, 'Data ProductPeriod Gagal Tersimpan')
        end
      end

      # PATCH/PUT /product_period/1
      def update
        if @product_period.update(product_period_params)
          response_updated(@product_period, 'Data ProductPeriod Terupdate')
        else
          response_error(@product_period, 'Data ProductPeriod Gagal Terupdate')
        end
      end

      # DELETE /product_period/1
      def destroy
        @product_period.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_product_period
        @product_period = ProductPeriod.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def product_period_params
        params.require(:product_period).permit(:province_id, :period_of_time, :due_date, :grace_period, :fine_amount, :fine_percentage, :auction_period)
      end
    end
  end
end
