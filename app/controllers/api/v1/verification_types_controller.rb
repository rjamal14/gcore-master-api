module Api
  module V1
    # Class Verifikasi Item
    class VerificationTypesController < ApplicationController
      before_action :set_verification_type, only: [:show, :update]

      # GET /verification_types
      def index
        verification_types = params[:search] ? VerificationType.where(name: /.*#{params[:search]}.*/i) : VerificationType

        verification_types = verification_types.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_verification_types = verification_types.map { |verification_type| VerificationTypeSerializer.new(verification_type, root: false) }

        response_index(verification_types, serial_verification_types)
      end

      def autocomplete
        verification_types = VerificationType.only(:id, :name)
                                             .where(name: /.*#{params[:query]}.*/i)
                                             .order_by(name: :asc)
                                             .limit(10)
        response_success(verification_types, 'Data yang ditemukan')
      end

      # GET /verification_types/1
      def show
        response_success(@verification_type, 'Data Verifikasi Type Ditemukan')
      end

      # POST /verification_types
      def create
        verification_type = VerificationType.new(verification_type_params.merge(created_by_id: current_user))
        if verification_type.save
          response_created(verification_type, 'Data Verifikasi Type tersimpan')
        else
          response_error(verification_type, 'Data Verifikasi Type gagal tersimpan')
        end
      end

      # PATCH/PUT /verification_types/1
      def update
        if @verification_type.update(verification_type_params.merge(updated_by_id: current_user))
          response_updated(@verification_type, 'Data Verivikasi Type terupdate')
        else
          response_error(@verification_type, 'Data Verifikasi Type gagal teupdate')
        end
      end

      # DELETE /verification_types/1
      def destroy
        if params.key?(:id)
          verification_types = VerificationType.in(id: params[:id].split(','))
          if verification_types.first.nil?
            response_not_found('Data Tidak ditemukan')
          else
            verification_types.destroy_all
            response_ok('Data terhapus')
          end
        else
          response_bad('Gagal Menghapus Data')
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_verification_type
        @verification_type = VerificationType.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def verification_type_params
        params.require(:verification_type).permit(:name, :status, verification_item_ids: [:verification_item_id])
      end
    end
  end
end
