module Api
  module V1
    # Bank Controller
    class DeviationsController < ApplicationController
      skip_before_action :current_owner, only: :deviation_getter

      def deviation_getter
        deviation = Transaction::DeviationService.new(params[:product_id], params[:insurance_item_id], params[:regional_id], params[:loan_amount])
        response_success(deviation.execute, 'Data Kosong')
      rescue StandardError => e
        response_bad(e)
      end
    end
  end
end
