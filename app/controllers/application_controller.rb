# App
class ApplicationController < ActionController::API
  before_action :current_owner
  before_action :set_notification
  include ActionController::Serialization
  include Response

  def auth
    @auth ||= Auth::AuthorizeService.new
  end

  def current_owner
    render json: { message: 'Unauthorize' }, status: :unauthorized unless request.headers['Authorization'].present?

    auth.token_setter(request.headers['Authorization'])
    current_owner ||= auth.current_user

    response_auth_failed if current_owner.nil?
    current_owner
  end

  def current_user
    current_owner.id if request.headers['Authorization'].present? && (current_owner != 'application')
  end

  def get_transaction(url, _params = '', _data = '')
    auth.get_transaction(url, _params, _data)
  end

  def set_notification
    request.env['exception_notifier.exception_data'] = { 'server' => request.env['SERVER_NAME'] }
  end
end
