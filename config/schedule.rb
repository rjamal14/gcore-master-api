# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
env :PATH, ENV['PATH']
set :output, '/home/khildamaililhaq/cron_log.log'
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 1.day, at: '00:01 pm' do
  rake 'ltv:update_status_by_date'
end

every 1.day, at: '00:01 pm' do
  rake 'slte:update_status_by_date'
end

every 1.day, at: '00:01 pm' do
  rake 'service_fee:update_status_by_date'
end

every 1.day, at: '12:00 am' do
  rake 'gold_price:update'
end
