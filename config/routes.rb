Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  namespace :api do
    namespace :v1 do
      get '/cities/index_by_provinces/', to: 'cities#index_by_province'
      get '/insurance_item_graphs', to: 'insurance_item#graph'
      get '/countries/autocomplete', to: 'countries#autocomplete'
      get '/provinces/autocomplete', to: 'provinces#autocomplete'
      get '/provinces/index_by_country', to: 'provinces#index_by_country'
      get '/provinces/autocomplete_country', to: 'provinces#autocomplete_by_country'
      get '/cities/autocomplete', to: 'cities#autocomplete'
      get '/unit_offices/autocomplete', to: 'unit_offices#autocomplete'
      get '/branch_offices/autocomplete', to: 'branch_offices#autocomplete'
      get '/banks/autocomplete', to: 'banks#autocomplete'
      get '/areas/autocomplete', to: 'areas#autocomplete'
      get '/regionals/autocomplete', to: 'regionals#autocomplete'
      get '/simulation/karat', to: 'carat_simulations#list'
      get '/verification_items/autocomplete', to: 'verification_items#autocomplete'
      get '/verification_types/autocomplete', to: 'verification_types#autocomplete'
      get '/customer_jobs/autocomplete', to: 'customer_jobs#autocomplete'
      get '/number_formats/autocomplete', to: 'number_formats#autocomplete'
      get '/number_formats/generate', to: 'number_formats#generate_number'
      get '/insurance_item/autocomplete', to: 'insurance_item#autocomplete'
      get '/gold_prices/', to: 'gold_prices#fetch_by_params'
      get '/admin_fees/', to: 'admin_fees#fetch_nominal'
      get '/due_dates/', to: 'due_dates#show_date'
      get '/product_insurance_item/autocomplete/', to: 'product_insurance_item#autocomplete'
      get '/estimate_values/', to: 'estimate_values#check_estimated_value'
      get '/rental_costs/', to: 'rental_costs#check_rental_cost_value'
      get '/product/:id/insurance_item', to: 'product#insurance_item_getter'
      get '/product/:product_id/deviation/:insurance_item_id/:regional_id/:loan_amount', to: 'deviations#deviation_getter'
      get '/districts/autocomplete', to: 'districts#autocomplete'
      get '/urbans/autocomplete', to: 'urbans#autocomplete'
      get '/address/', to: 'address#data_getter'
      post '/address/find', to: 'address#find'
      get '/provinces/master_data/:id', to: 'provinces#master_data'
      get '/city/multi/', to: 'cities#array_city'
      get '/money_bills/autocomplete', to: 'money_bills#autocomplete'
      get '/money_bills/list', to: 'money_bills#list_all_money_bills'
      get '/product/autocomplete', to: 'product#autocomplete'
      post '/installment/attribute', to: 'installment_simulations#check_installment_attribute'
      post '/installment/simulate', to: 'installment_simulations#simulate_installment'
      get '/transaction_costs', to: 'transaction_costs#transaction_cost_getter'
      get '/areas/logo_company', to: 'areas#logo_company'
      get 'notifications/unread', to: 'notifications#unread_index'

      resources :countries, :cities, :provinces, :branch_offices, :unit_offices, :banks, :regionals, :areas,
                :customer_jobs, :number_formats, :ltv, :verification_items, :slte, :service_fee, :verification_types,
                :insurance_item, :office_hour_schedules, :product_finances, :product_finance_items, :gold_market_price,
                :head_offices, :offices, :urbans, :districts, :money_bills
      resources :product do
        member do
          get 'prolongation', to: 'product#prolongation_getter'
          get 'fine_detail', to: 'product#fine_product_getter'
        end
        resources :product_period, :product_rental_cost, :product_rental_cost_discount, :product_admin_cost,
                  :product_admin_cost_discount, :product_deviation_setup, :product_deviation_item, :product_one_obligor,
                  :product_insurance_item
      end
      resources :notifications, only: [:index, :show, :destroy]
    end
  end
  mount ActionCable.server => '/cable'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
