CarrierWave.configure do |config|
  config.storage = :aliyun
  config.aliyun_access_key_id = ENV['aliyun_access_key_id']
  config.aliyun_access_key_secret = ENV['aliyun_access_key_secret']
  config.aliyun_bucket = ENV['aliyun_bucket']
  config.aliyun_internal = ENV['aliyun_internal']
  config.aliyun_region = ENV['aliyun_region']
  config.aliyun_host = ENV['aliyun_host']
  config.aliyun_mode = ENV['aliyun_mode']
end
