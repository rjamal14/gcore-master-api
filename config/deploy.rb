# lock '~> 3.15.0'

set :application, 'gcda-master-api'
set :repo_url, 'git@bitbucket.org:c-aio/gcore-master-api.git'
set :deploy_to, '/home/deploy/apps/gcda-master-api'
append :linked_files, 'config/master.key', 'config/mongoid.yml', '.env'
append :linked_dirs, 'public/uploads'
set :keep_releases, 5
set :rbenv_type, :user # or :system, or :fullstaq (for Fullstaq Ruby), depends on your rbenv setup
set :rbenv_ruby, '2.6.4'

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end
end

namespace :sneakers do
  desc 'Update the system rules '
  task :run do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute "kill -9 $(ps aux | grep gcda-master-api/shared/bundle | awk '{print $2}')"
          execute :rake, 'sneakers:run'
        end
      end
    end
  end
end
